package pdf

import (
	"bytes"
	"fmt"
	"image/jpeg"
	"io"
	"strings"
	"time"
)

// A PDF manages the creation of a PDF document.  It manages information about
// the pages in a document, along with those pages' contents.  Additional
// required resources, such as images, fonts, and shadings are also managed.
//
// Each PDF maintains an index to the current page, which is used to direct
// all drawing operations.  Drawing operations are append only,
// so old drawing operations cannot not be queried or modified.  However, old pages
// can be reopened and added to.
//
// Drawing operations affect the current page.  Methods that modify the graphics state or
// create drawing operations will panic if no page is currently selected.
type PDF struct {
	version            Version
	disableCompression bool
	defPageSize        Size
	pages              []page
	pageSizes          map[int]Size
	currentPage        *page
	metadata           struct {
		title    string
		subject  string
		author   string
		keywords string
		creator  string
		creation time.Time
	}
	// Resources
	bookmarks []bookmarkResource
	images    []imageResource
	gradients []gradientResource

	// Object
	objects []int

	// Fonts
	fonts       []Font
	currentFont struct {
		font      Font
		fontSize  PT
		underline bool
		up        int
		ut        int
	}

	// Graphics States
	gstates []gstateResource

	// Encryption
	encrypt struct {
		revision      uint
		encryptionKey [16]byte
		encryptionO   [32]byte
		encryptionU   [32]byte
		permissions   Permissions
	}

	// Interactive forms
	fields []field
}

type page struct {
	bytes.Buffer
	index  int
	annots []annotation
}

type gstateResource struct {
	BM BlendMode
	CA float32
	ca float32
}

// New creates a new PDF document.  The default page size must be specified.
func New(size Size) *PDF {
	return &PDF{
		version:     Version{1, 3},
		defPageSize: size,
		pages:       make([]page, 0, 8),
		pageSizes:   make(map[int]Size),
	}
}

// AddJPEG embeds a JPEG image into the PDF document.  Note that the image
// is not actually displayed on any page.  However, once created, the image
// can be used multiple times.  See the method ImageWithIndex.
func (p *PDF) AddJPEG(w io.Reader) (imageIndex int, err error) {
	// Make a copy of the bytes for the JPEG image
	buffer := bytes.NewBuffer(nil)
	_, err = buffer.ReadFrom(w)
	if err != nil {
		return 0, err
	}

	// Get configuration information from the image
	config, err := jpeg.DecodeConfig(bytes.NewReader(buffer.Bytes()))
	if err != nil {
		return 0, err
	}

	// Add the image resource to the PDF
	p.images = append(p.images, imageResource{
		config: config,
		data:   buffer.Bytes(),
		filter: "DCTDecode",
	})

	return len(p.images), nil
}

// AddPage appends a new page to the document using the default page size.
// The new page will be opened for drawing.
// The page index for the new page will be returned.
func (p *PDF) AddPage() int {
	return p.AddPageWithSize(p.defPageSize)
}

// AddPageWithSize appends a new page to the document using the specified page size.
// The new page will be opened for drawing.
// The page index for the new page will be returned.
func (p *PDF) AddPageWithSize(size Size) int {
	p.ClosePage()

	// Start new page
	ndx := len(p.pages)
	p.pages = append(p.pages, page{
		index: ndx,
	})
	if size != p.defPageSize {
		p.pageSizes[ndx] = size
	}

	// Move to the new page
	p.currentPage = &p.pages[ndx]
	return ndx
}

// ClosePage closes the current page.
func (p *PDF) ClosePage() {
	if p.currentPage == nil {
		// No page open.
		return
	}

	p.currentPage = nil
}

// Close finishes processing of the description of the PDF document, and
// returns a buffer with the data for the PDF document.
func (p *PDF) Close() (*bytes.Buffer, error) {
	// If there are any gradients, we need to be at least 1.3
	if len(p.gradients) > 0 && p.version.LT(1, 3) {
		return nil, ErrRequiredVersion
	}
	// If there are any blend modes, we need to support at least version 1.4
	if len(p.gstates) > 0 && p.version.LT(1, 4) {
		return nil, ErrRequiredVersion
	}

	// If there are no pages, add a page so that there is at least one.
	if len(p.pages) == 0 {
		p.AddPage()
	}
	// If there is a page open, close it.
	p.ClosePage()

	// Initialize the array used for tracking the offset of objects
	// preallocate 2 objects, root page and resources
	p.objects = []int{0, 0}
	defer func() {
		p.objects = nil
	}()

	w := bytes.NewBuffer(nil)
	p.writeHeader(w)
	p.writePages(w, 3+len(p.pages)*2 /* fieldsIndex */)
	fieldsIndex := p.writeFields(w)
	p.writeResources(w)
	encryptNdx := p.writeEncrypt(w)
	bookmarksIndex := p.writeBookmarks(w)
	p.writeInfo(w)
	p.writeCatalog(w, bookmarksIndex, fieldsIndex)
	offset := p.writeXref(w)
	p.writeTrailer(w, encryptNdx)

	// Start Xref
	w.WriteString("startxref\n")
	fmt.Fprintf(w, "%d\n", offset)
	w.WriteString("%%EOF\n")

	return w, nil
}

// CurrentPageIndex returns the index of the currently open page.  The page index is
// 0-based, and should be used in calls to MoveToPage.
//
// If no page is currently open, then the value of ok will be false.
func (p *PDF) CurrentPageIndex() (page int, ok bool) {
	if p.currentPage == nil {
		return 0, false
	}
	return p.currentPage.index, true
}

// CurrentPageSize returns the size of the current page.
// If no page is current, then the default page size for the document is returned.
func (p *PDF) CurrentPageSize() Size {
	// No current page, return the default
	if p.currentPage == nil {
		return p.defPageSize
	}

	// Do we have a custom page size for the current page?
	ndx := p.currentPage.index
	if ps, ok := p.pageSizes[ndx]; ok {
		return ps
	}

	return p.defPageSize
}

// DisableCompression disables the compression of data streams inside the PDF
// document.
func (p *PDF) DisableCompression() {
	p.disableCompression = true
}

// ForEachPage moves through each page in the PDF document, and calls
// the specified callback.
func (p *PDF) ForEachPage(draw func(pageIndex int)) {
	for i := range p.pages {
		p.MoveToPage(i)
		draw(i)
	}

	p.ClosePage()
}

// MoveToPage closes the current page, and then selects the page with
// the specified page index.  Future drawing command will then be appended
// to that page.
func (p *PDF) MoveToPage(pageIndex int) {
	p.ClosePage()

	if pageIndex >= len(p.pages) {
		panic("Index for page is out of range")
	}
	p.currentPage = &p.pages[pageIndex]
}

// PageCount returns the number of pages in the document.
func (p *PDF) PageCount() int {
	return len(p.pages)
}

// SetAuthor sets the author for the document, which appears in the
// properties for the document.
func (p *PDF) SetAuthor(author string) {
	p.metadata.author = strings.TrimSpace(author)
}

// SetCreator sets the creator for the document, which appears in the
// properties for the document.
func (p *PDF) SetCreator(creator string) {
	p.metadata.creator = strings.TrimSpace(creator)
}

// SetCreationDate sets the creation date for the document, which appears
// in the properties for the document.  If the creation date is zero,
// it will be replaced with the current time when the document is closed.
func (p *PDF) SetCreationDate(creation time.Time) {
	p.metadata.creation = creation
}

// SetKeywords sets the keywords for the document, which appears in the
// properties for the document.
func (p *PDF) SetKeywords(keywords string) {
	p.metadata.keywords = strings.TrimSpace(keywords)
}

// SetSubject sets the subject for the document, which appears in the
// properties for the document.
func (p *PDF) SetSubject(subject string) {
	p.metadata.subject = strings.TrimSpace(subject)
}

// SetTitle sets the title for the document, which appears in the
// properties for the document.
func (p *PDF) SetTitle(title string) {
	p.metadata.title = strings.TrimSpace(title)
}

// Version returns the version information for this document.  Some operations
// may require a higher level, so the version may be increased when required
// to support features.
func (p *PDF) Version() Version {
	return p.version
}

// SetMinimumVersion compares the requested version to the current version of
// the PDF document, and increases the version if necessary.
func (p *PDF) SetMinimumVersion(v Version) {
	if p.version.LT(v.Major, v.Minor) {
		p.version = v
	}
}
