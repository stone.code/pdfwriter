package pdf

import (
	"testing"
)

func TestBlend(t *testing.T) {
	pdf := New(Size{1 * Inch, 1 * Inch})
	if out := pdf.Version(); !out.EQ(1, 3) {
		t.Errorf("Unexpected version for PDF document, %s", out)
	}
	pdf.SetMinimumVersion(Version{1, 4})
	if out := pdf.Version(); !out.EQ(1, 4) {
		t.Errorf("Unexpected version for PDF document, %s", out)
	}
	pdf.DisableCompression()

	// Create an opaque blend mode to be used for printing text over the
	// blended images.
	opaque := pdf.AddBlendMode(BlendNormal, 1, 1)

	for i, v := range []BlendMode{
		BlendNormal,
		BlendMultiply,
		BlendScreen,
		BlendOverlay,
		BlendDarken,
		BlendLighten,
		BlendColorDodge,
		BlendColorBurn,
		BlendHardLight,
		BlendSoftLight,
		BlendDifference,
		BlendExclusion,
		BlendHue,
		BlendSaturation,
		BlendColor,
		BlendLuminosity,
	} {
		pdf.AddPage()
		pdf.SetBlendMode(v, 0.75, 0.75)
		if len(pdf.gstates) != i+2 {
			t.Errorf("inconsitency found in the number of graphic states")
		}
		pdf.SetFillRGB(0xff, 0, 0)
		pdf.Rectangle(0, 0, 0.667*Inch, 1*Inch, Fill)
		pdf.SetFillRGB(0, 0, 0xff)
		pdf.Rectangle(0.333*Inch, 0, 0.667*Inch, 1*Inch, Fill)

		pdf.SetBlendModeWithIndex(opaque)
		pdf.SetFillRGB(0, 0, 0)
		mustSetFont(t, pdf, Helvetica, Normal, 8)
		pdf.Text(v.postscript(), 6, 72-12)
	}

	// Close and write the PDF file.
	writeFile(t, pdf)
}
