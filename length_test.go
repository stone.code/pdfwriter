package pdf

import (
	"fmt"
	"math"
	"testing"
)

func approxEqOne(in float64) bool {
	return in < math.Nextafter(1, 0) && math.Nextafter(1, 2) < in
}

func TestPT(t *testing.T) {
	if out := (1 * Pt).Points(); approxEqOne(out) {
		t.Errorf("Problem in conversion to or from points, got %f.", out)
	}
	if out := (1 * Inch).Inches(); approxEqOne(out) {
		t.Errorf("Problem in conversion to or from inches, got %f.", out)
	}
	if out := (1 * Cm).Cm(); approxEqOne(out) {
		t.Errorf("Problem in conversion to or from centimeters, got %f.", out)
	}
	if out := (1 * Mm).Mm(); approxEqOne(out) {
		t.Errorf("Problem in conversion to or from millimeters, got %f.", out)
	}
	if out := (1 * Pica).Pica(); approxEqOne(out) {
		t.Errorf("Problem in conversion to or from pica, got %f.", out)
	}
}

func ExamplePT() {
	length := 72 * Pt

	fmt.Printf("72 points is:\n")
	fmt.Printf("  %.1f inches\n", length.Inches())
	fmt.Printf("  %.1f centimeters\n", length.Cm())
	fmt.Printf("  %.1f millimeters\n", length.Mm())
	fmt.Printf("  %.1f pica\n", length.Pica())

	// Output:
	// 72 points is:
	//   1.0 inches
	//   2.5 centimeters
	//   25.4 millimeters
	//   6.0 pica
}

func TestPoint(t *testing.T) {
	cases := []struct {
		in  Point
		out string
	}{
		{Point{1, 2}, "(1,2)"},
	}

	for i, v := range cases {
		if out := v.in.String(); out != v.out {
			t.Errorf("Problem in string conversion of point (%d), want %s, got %s", i, v.out, out)
		}
	}
}

func ExampleRect() {
	// The following rectangle will have to have the corners flipped.
	r := Rect(7.5*Inch, 10*Inch, 1*Inch, 1*Inch)
	fmt.Printf("The rectangle is %s.\n", r)

	// Output:
	// The rectangle is (72,72)-(540,720).
}

func TestRectangle(t *testing.T) {
	cases := []struct {
		in    Rectangle
		canon Rectangle
		size  Point
	}{
		{Rectangle{Point{0, 0}, Point{72, 72}}, Rectangle{Point{0, 0}, Point{72, 72}}, Point{72, 72}},
		{Rectangle{Point{72, 72}, Point{0, 0}}, Rectangle{Point{0, 0}, Point{72, 72}}, Point{-72, -72}},
	}

	for i, v := range cases {
		if out := v.in.Canon(); out != v.canon {
			t.Errorf("Case %d, expected %v, got %v", i, v.canon, out)
		}
		if out := v.in.Size(); out != v.size {
			t.Errorf("Case %d, expected %v, got %v", i, v.size, out)
		}
	}
}
