package pdf

// PageOrientation represents the direction in which a document is printed
// or displayed.
type PageOrientation uint8

// The possible page orientations.
const (
	Portrait  PageOrientation = 0 // Page oriented so that width < height.
	Landscape PageOrientation = 1 // Page oriented so that height < width.
)

// A Size is the width and height of a page.
type Size struct {
	Width, Height PT
}

// Portrait returns the dimensions of a similar page size, but with a portrait orientation.
func (s Size) Portrait() Size {
	if s.Width < s.Height {
		return s
	}
	return Size{s.Height, s.Width}
}

// Landscape returns the dimensions of a similar page size, but with a landscape orientation.
func (s Size) Landscape() Size {
	if s.Width < s.Height {
		return Size{s.Height, s.Width}
	}
	return s
}

// Orientation determines the orientation of the page size.
func (s Size) Orientation() PageOrientation {
	if s.Width <= s.Height {
		return Portrait
	}
	return Landscape
}

// String returns the name of the page size, if recognized.
// For page sizes that are not recognized, the page size will be described as "Custom".
func (s Size) String() string {
	if s.Width > s.Height {
		s.Width, s.Height = s.Height, s.Width
	}

	if s == Letter {
		return "Letter"
	}
	if s == Legal {
		return "Legal"
	}
	if s == Tabloid {
		return "Tabloid"
	}
	if s == A1 {
		return "A1"
	}
	if s == A2 {
		return "A2"
	}
	if s == A3 {
		return "A3"
	}
	if s == A4 {
		return "A4"
	}
	if s == A5 {
		return "A5"
	}
	if s == A6 {
		return "A6"
	}
	return "Custom"
}

// Common page sizes.
var (
	A3      = Size{841.89, 1190.55}
	A4      = Size{595.28, 841.89}
	A5      = Size{420.94, 595.28}
	A6      = Size{297.64, 420.94}
	A2      = Size{1190.55, 1683.78}
	A1      = Size{1683.78, 2383.94}
	Letter  = Size{612, 792}
	Legal   = Size{612, 1008}
	Tabloid = Size{792, 1224}
)
