package pdf

// AddExternalLink adds a link area to the current page.  When clicked, the
// link area will redirect to the external resources indicated by the URL.
//
// This function will panic if no page is currently open.  See the method CurrentPage.
func (p *PDF) AddExternalLink(bounds Rectangle, url string) {
	p.currentPage.annots = append(p.currentPage.annots, annotation{
		kind:   annotationLinkURL,
		bounds: bounds,
		url:    url,
	})
}

// AddInternalLink adds a link area to the current page.  When clicked, the
// link area will redirect to another location in the document, specified by
// the pageIndex and the x, y position on that page.
//
// This function will panic if no page is currently open.  See the method CurrentPage.
func (p *PDF) AddInternalLink(bounds Rectangle, pageIndex int, target Point) {
	p.currentPage.annots = append(p.currentPage.annots, annotation{
		kind:      annotationLink,
		bounds:    bounds,
		pageIndex: pageIndex, target: target,
	})
}

// AddTextAnnotation adds a note attached to a point on the current page.
//
// This function will panic if no page is currently open.  See the method CurrentPage.
func (p *PDF) AddTextAnnotation(bounds Rectangle, contents string) {
	p.currentPage.annots = append(p.currentPage.annots, annotation{
		kind:     annotationText,
		bounds:   bounds,
		contents: contents,
	})
}

// AddCheckboxField adds a checkbox to the curent page.
//
// This function will panic if no page is currently open.  See the method CurrentPage.
func (p *PDF) AddCheckboxField(bounds Rectangle, name string, value bool) {
	p.currentPage.annots = append(p.currentPage.annots, annotation{
		kind:   annotationWidget,
		bounds: bounds,
		field:  len(p.fields),
	})

	s := ""
	if value {
		s = " "
	}
	p.fields = append(p.fields, field{kind: fieldCheckbox, bounds: bounds, name: name, value: s})
}

// TextFieldOptions contains flags affecting the behaviour of text fields.
// See section 8.6.3 of the reference.
type TextFieldOptions uint32

// All field options for text fields.
const (
	TextFieldNoOpt           TextFieldOptions = 0
	TextFieldReadOnly        TextFieldOptions = (1 << 0)
	TextFieldRequired        TextFieldOptions = (1 << 1)
	TextFieldNoExport        TextFieldOptions = (1 << 2)
	TextFieldMultiline       TextFieldOptions = (1 << 12)
	TextFieldPassword        TextFieldOptions = (1 << 13)
	TextFieldFileSelect      TextFieldOptions = (1 << 20) // Requires PDF version 1.4.
	TextFieldDoNotSpellCheck TextFieldOptions = (1 << 22) // Requires PDF version 1.4.
	TextFieldDoNotScroll     TextFieldOptions = (1 << 23) // Requires PDF version 1.4.
)

// AddTextField adds a text field to the current page.
//
// This function will panic if no page is currently open.  See the method CurrentPage.
func (p *PDF) AddTextField(bounds Rectangle, name string, value string, options TextFieldOptions, maxlen int) {
	p.currentPage.annots = append(p.currentPage.annots, annotation{
		kind:   annotationWidget,
		bounds: bounds,
		field:  len(p.fields),
	})

	p.fields = append(p.fields, field{kind: fieldText, bounds: bounds, name: name, value: value, flags: uint32(options), maxlen: maxlen})
}

// AddSignatureField adds a signature field to the current page.
//
// This function will panic if no page is currently open.  See the method CurrentPage.
func (p *PDF) AddSignatureField(bounds Rectangle, name string) {
	p.currentPage.annots = append(p.currentPage.annots, annotation{
		kind:   annotationWidget,
		bounds: bounds,
		field:  len(p.fields),
	})

	p.fields = append(p.fields, field{kind: fieldSignature, bounds: bounds, name: name})
}

type annotationKind uint8

const (
	annotationText annotationKind = iota
	annotationLink
	annotationLinkURL
	annotationWidget
)

type annotation struct {
	kind      annotationKind
	bounds    Rectangle
	contents  string
	pageIndex int
	target    Point
	url       string
	field     int
}

type fieldKind int8

const (
	fieldPushbutton fieldKind = iota
	fieldCheckbox
	fieldRadiobutton
	fieldText
	fieldChoice
	fieldSignature
)

type field struct {
	kind   fieldKind
	bounds Rectangle
	name   string
	value  string
	maxlen int
	flags  uint32
}
