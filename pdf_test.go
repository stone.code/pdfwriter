package pdf

import (
	"fmt"
	"os"
	"testing"
	"time"
)

const (
	helloASCII  = "Hello, world!"   // Example text only using ASCII
	helloLATIN8 = "Bonjour, ça va?" // Example text using LATIN-8 characters
	helloUTF8   = "Hello, 世界"       // Example text requiring full UTF-8
	helloZD     = "Hello, ♣♦♥♠"     // Example using the ZapfDingbats
)

func writeFile(t *testing.T, doc *PDF) {
	data, err := doc.Close()
	if err != nil {
		t.Fatalf("Error, %s", err)
	}

	file, err := os.OpenFile("./"+t.Name()+".pdf", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0666)
	if err != nil {
		t.Fatalf("Could not open file, %s", err)
	}
	defer file.Close()

	_, err = data.WriteTo(file)
	if err != nil {
		t.Fatalf("Could not write file, %s", err)
	}

	offset, err := file.Seek(0, 2)
	if err != nil {
		t.Fatalf("Could not seek file, %s", err)
	}
	t.Logf("File size for %s is %d\n", t.Name(), offset)
}

func TestAddPage(t *testing.T) {
	pageSize := Size{1 * Inch, 1 * Inch}

	pdf := New(pageSize)
	if out := pdf.CurrentPageSize(); out != pageSize {
		t.Errorf("Unexpected default page size, %s", out)
	}

	pdf.AddPage() // page with standard size
	if ndx, ok := pdf.CurrentPageIndex(); ndx != 0 || !ok {
		t.Errorf("Unexpected page index, got %d, %v", ndx, ok)
	}
	if out := pdf.CurrentPageSize(); out != pageSize {
		t.Errorf("Unexpected default page size, %s", out)
	}
	pdf.ClosePage()
	if ndx, ok := pdf.CurrentPageIndex(); ok {
		t.Errorf("Unexpected page index, got %d, %v", ndx, ok)

	}
	pdf.AddPageWithSize(Letter) // page with custom size
	if ndx, ok := pdf.CurrentPageIndex(); ndx != 1 || !ok {
		t.Errorf("Unexpected page index, got %d, %v", ndx, ok)
	}
	if out := pdf.CurrentPageSize(); out != Letter {
		t.Errorf("Unexpected default page size, %s", out)
	}

	pdf.AddPage() // page with standard size
	if ndx, ok := pdf.CurrentPageIndex(); ndx != 2 || !ok {
		t.Errorf("Unexpected page index, got %d, %v", ndx, ok)
	}
	if out := pdf.CurrentPageSize(); out != pageSize {
		t.Errorf("Unexpected default page size, %s", out)
	}

	// Close and write the PDF file.
	writeFile(t, pdf)

	// Check properties after close
	if pdf.PageCount() != 3 {
		t.Errorf("Incorrect page count at end of test")
	}
	if ndx, ok := pdf.CurrentPageIndex(); ok {
		t.Errorf("Unexpected page index, got %d, %v", ndx, ok)
	}
}

func TestText(t *testing.T) {
	const fontSize = 12 * Pt

	pdf := New(Size{2 * Inch, 2.5 * Inch})
	pdf.SetTitle(helloASCII)
	pdf.SetSubject(helloASCII + " )")
	pdf.SetKeywords(helloLATIN8)
	pdf.SetAuthor(helloUTF8)
	pdf.SetCreator(helloUTF8 + " )")

	title := func(s string) {
		mustSetFont(t, pdf, Helvetica, Normal, fontSize)
		pdf.Text(s, fontSize, 2.5*Inch-fontSize)
	}

	styles := []FontStyle{Normal, Italic, Bold, Bold | Italic, Underline, Italic | Underline, Bold | Underline}

	pdf.AddPageWithSize(Size{3 * Inch, 2.5 * Inch})
	title("Built in fonts")
	pdf.SetDrawRGB(0, 0, 255)
	for i, v := range styles {
		mustSetFont(t, pdf, Times, v, fontSize)
		pdf.Text("Times!", fontSize, 2*Inch-fontSize.Scale(i, 1))
	}
	for i, v := range styles {
		mustSetFont(t, pdf, Helvetica, v, fontSize)
		pdf.Text("Helvetica", 6*fontSize, 2*Inch-fontSize.Scale(i, 1))
	}
	for i, v := range styles {
		mustSetFont(t, pdf, Courier, v, fontSize)
		pdf.Text("Courier", 12*fontSize, 2*Inch-fontSize.Scale(i, 1))
	}

	// Use of text flow
	pdf.AddPage()
	title("Reflow")
	mustSetFont(t, pdf, Times, Normal, fontSize)
	pdf.TextFlow("This is a test of text reflow.  How well will it work?", 16, 2*Inch-16, 2*Inch-32)
	pdf.SetDrawGray(128)
	pdf.Line(Point{16, 0}, Point{16, 2 * Inch})
	pdf.Line(Point{2*Inch - 16, 0}, Point{2*Inch - 16, 2 * Inch})

	// Use of text split
	pdf.AddPage()
	title("Reflow using Split")
	ndx, err := pdf.SetFont(Times, Normal, fontSize)
	if err != nil {
		t.Fatalf("could not set font: %s", err)
	}
	pdf.SetDrawGray(128)
	pdf.Line(Point{16, 0}, Point{16, 2 * Inch})
	pdf.Line(Point{2*Inch - 16, 0}, Point{2*Inch - 16, 2 * Inch})
	lines := pdf.TextSplit("This is a test of text reflow.  How well will it work?", ndx, fontSize, 2*Inch-32)
	for i, v := range lines {
		pdf.Text(v, 16, 2*Inch-16-fontSize.Scale(i*3, 2))
	}

	// Use of text split and justification
	pdf.AddPage()
	title("Reflow / Adjust spacing")
	ndx, _ = pdf.SetFont(Times, Normal, fontSize)
	pdf.SetDrawGray(128)
	pdf.Line(Point{16, 0}, Point{16, 2 * Inch})
	pdf.Line(Point{2*Inch - 16, 0}, Point{2*Inch - 16, 2 * Inch})
	lines = pdf.TextSplit("This is a test of text reflow.  How well will it work?", ndx, fontSize, 2*Inch-32)
	for i, v := range lines {
		if i < len(lines)-1 {
			w, _ := pdf.TextWidth(v)
			pdf.SetCharacterSpacing(float32(2*Inch-32-w) / float32(len(v)-1))
		} else {
			pdf.SetCharacterSpacing(0)
		}
		pdf.Text(v, 16, 2*Inch-16-fontSize.Scale(i*3, 2))
	}

	// Check writing text with different encoding requirements
	pdf.AddPage()
	title("Text Encoding")
	mustSetFont(t, pdf, Times, Normal, fontSize)
	ok := pdf.Text(helloASCII, 16, 2*Inch-fontSize)
	if !ok {
		t.Errorf("Failed to encode string when displaying text")
	}
	mustSetFont(t, pdf, Times, Normal, fontSize)
	ok = pdf.Text(helloLATIN8, 16, 2*Inch-fontSize-1.5*fontSize)
	if !ok {
		t.Errorf("Failed to encode string when displaying text")
	}
	ok = pdf.Text(helloUTF8, 16, 2*Inch-fontSize-3*fontSize)
	if ok {
		t.Errorf("Unexpected success when encoding string to display text")
	}
	mustSetFont(t, pdf, Dingbats, Normal, fontSize)
	ok = pdf.Text(helloZD, 16, 2*Inch-fontSize-4.5*fontSize)
	if ok {
		t.Errorf("Unexpected success when encoding string to display text")
	}

	pdf.AddPage()
	title("Text Renderer")
	pdf.SetFillGray(128)
	pdf.SetDrawRGB(255, 0, 0)
	pdf.SetLineWidth(0.5)
	mustSetFont(t, pdf, Helvetica, Bold, fontSize)
	pdf.Text("ABCDEFG", fontSize, 2*Inch-1.5*fontSize)
	pdf.SetTextRender(FillAndStrokeText)
	pdf.Text("ABCDEFG", fontSize, 2*Inch-3*fontSize)
	pdf.SetTextRender(StrokeText)
	pdf.Text("ABCDEFG", fontSize, 2*Inch-4.5*fontSize)
	pdf.SetJoin(RoundJoin)
	pdf.Text("ABCDEFG", fontSize, 2*Inch-6*fontSize)

	pdf.AddPage()
	title("Spacing")
	mustSetFont(t, pdf, Helvetica, Bold, fontSize)
	pdf.SetWordSpacing(8)
	pdf.Text("ABC DEF", fontSize, 2*Inch-1.5*fontSize)
	pdf.SetWordSpacing(0)
	pdf.SetCharacterSpacing(4)
	pdf.Text("ABC DEF", fontSize, 2*Inch-3*fontSize)
	pdf.SetWordSpacing(0)
	pdf.SetCharacterSpacing(0)
	pdf.SetTextScaling(150)
	pdf.Text("ABC DEF", fontSize, 2*Inch-4.5*fontSize)

	// Close and write the PDF file.
	writeFile(t, pdf)
}

func TestPDF_ForEachPage(t *testing.T) {
	const fontSize = 24 * Pt

	doc := New(Letter)

	// Page 1
	doc.AddPage()
	mustSetFont(t, doc, Times, Normal, fontSize)
	doc.Text("Some text on the first page...", 1*Inch, 10*Inch-fontSize)
	doc.Text("You should see a nice footer on each page.", 1*Inch, 10*Inch-fontSize.Scale(5, 2))

	// Page 2
	doc.AddPage()
	mustSetFont(t, doc, Times, Normal, fontSize)
	doc.Text("Some text on the second page...", 1*Inch, 10*Inch-fontSize)

	// Page 3
	doc.AddPage()
	mustSetFont(t, doc, Times, Normal, fontSize)
	doc.Text("Some text on the third page...", 1*Inch, 10*Inch-fontSize)

	doc.ForEachPage(func(pageIndex int) {
		const fontSize = 12 * Pt
		mustSetFont(t, doc, Times, Normal, fontSize)
		doc.Text(fmt.Sprintf("Page %d of 3", pageIndex+1), 1*Inch, 1*Inch)
	})

	// Close and write the PDF file.
	writeFile(t, doc)
}

func ExamplePDF_SetAuthor() {
	doc := New(Letter.Portrait())
	doc.SetAuthor("Your Name")
	doc.SetCreationDate(time.Date(2006, 1, 2, 15, 4, 5, 0, time.UTC)) // For reproducibility of the test output
	data, err := doc.Close()
	if err != nil {
		fmt.Printf("error: could not close PDF document: %s\n", err)
	} else {
		fmt.Printf("%s\n", data)
	}
}

func ExamplePDF() {
	const Margin = 1 * Inch

	// Create a new PDF document using Letter paper, and some dark blue text
	// over a gray background.
	doc := New(Letter)
	doc.AddPage()
	ps := doc.CurrentPageSize()
	doc.SetFillGray(192)
	doc.Rectangle(Margin, Margin, ps.Width-2*Margin, ps.Height-2*Margin, Fill)
	doc.SetFillRGB(0, 0, 128)
	_, _ = doc.SetFont(Times, Normal, 12*Pt)
	doc.Text("Hello, world!", Margin, ps.Height-Margin-12)

	// Write the file to disk
	data, err := doc.Close()
	if err != nil {
		fmt.Printf("error: could not close PDF document: %s\n", err)
	} else {
		// Omitting error handling for example.
		file, _ := os.OpenFile("./example.pdf", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0666)
		defer file.Close()
		_, err := data.WriteTo(file)
		if err != nil {
			fmt.Printf("error: could not write file: %s\n", err)
		}
	}
}

func ExamplePDF_AddPageWithSize() {
	// Create a new PDF document with a default page size of Letter.
	doc := New(Letter)
	doc.AddPage()              // First page
	doc.AddPageWithSize(Legal) // Second page
	doc.AddPage()              // Third page

	// Verify the page sizes.
	for i := 0; i < doc.PageCount(); i++ {
		doc.MoveToPage(i)
		fmt.Printf("Page %d has size %s.\n", i+1, doc.CurrentPageSize())
	}

	// Output:
	// Page 1 has size Letter.
	// Page 2 has size Legal.
	// Page 3 has size Letter.
}

func ExamplePDF_ForEachPage() {
	// Create a new PDF document, with some pages.
	doc := New(Letter)
	doc.AddPage()
	doc.SetFillRGB(192, 0, 0)
	doc.Rectangle(0, 0, Letter.Width, Letter.Height, Fill|Stroke)
	doc.AddPage()
	doc.SetFillRGB(0, 192, 0)
	doc.Rectangle(0, 0, Letter.Width, Letter.Height, Fill|Stroke)
	doc.AddPage()
	doc.SetFillRGB(0, 0, 192)
	doc.Rectangle(0, 0, Letter.Width, Letter.Height, Fill|Stroke)

	// Add a footer to each page
	doc.ForEachPage(func(index int) {
		doc.SetFillGray(0)
		_, _ = doc.SetFont(Times, Normal, 10)
		doc.Text(
			fmt.Sprintf("Page %d of %d", index+1, doc.PageCount()),
			72, 72,
		)
	})

	// Write the file to disk
	data, err := doc.Close()
	if err != nil {
		fmt.Printf("error: could not close pdf: %s\n", err)
	} else {
		// Omitting error handling for example.
		file, _ := os.OpenFile("./example.pdf", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0666)
		defer file.Close()
		_, err = data.WriteTo(file)
		if err != nil {
			fmt.Printf("error: could not write file: %s\n", err)
		}
	}
}
