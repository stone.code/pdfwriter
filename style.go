package pdf

// CapStyle represents a line-cap, which describe the shape for drawing the ends of lines.
// See section 8.4.3.3.
type CapStyle uint8

// Available line-cap styles.
const (
	ButtCap   CapStyle = 0
	RoundCap  CapStyle = 1
	SquareCap CapStyle = 2
)

// JoinStyle represents different methods for drawing the joins in line segments.
// See section 8.4.3.4.
type JoinStyle uint8

// Available line-join styles.
const (
	MiterJoin JoinStyle = 0
	RoundJoin JoinStyle = 1
	BevelJoin JoinStyle = 2
)

// RenderingIntent represents specifies how rendering devices should handle colour accuracy.
// See section 8.6.5.8.
type RenderingIntent string

// Available rendering intents.
const (
	AbsoluteColorimetric RenderingIntent = "AbsoluteColorimetric"
	RelativeColorimetric RenderingIntent = "RelativeColorimetric"
	Saturation           RenderingIntent = "Saturation"
	Perceptual           RenderingIntent = "Perceptual"
)
