package pdf

import (
	"bytes"
	"compress/zlib"
	"fmt"
)

func (p *PDF) newobj(w *bytes.Buffer) int {
	// Add the current byte position as the offset for a new object
	retval := len(p.objects) + 1
	p.objects = append(p.objects, w.Len())
	fmt.Fprintf(w, "%d 0 obj\n", retval)
	return retval
}

func (*PDF) endobj(w *bytes.Buffer) {
	w.WriteString("endobj\n")
}

func (p *PDF) putstream(w *bytes.Buffer, data []byte) {
	// Encrypt the data.  This will only modify the data
	// if encryption is set for this PDF.
	p.encryptData(data, len(p.objects))

	// Write the stream object.
	w.WriteString("stream\n")
	w.Write(data)
	w.WriteString("endstream\n")
}

func compress(data []byte, disable bool) ([]byte, string) {
	// Short-circuit compression?
	if disable || len(data) <= 64 {
		return data, ""
	}

	// Compress the data.
	// Note that we can ignore errors since we are writing into a bytes.Buffer.
	// To match interfaces, the Write methods do have an error in the return,
	// but they will always be nil.
	var buffer bytes.Buffer
	cmp, _ := zlib.NewWriterLevel(&buffer, zlib.BestSpeed)
	_, _ = cmp.Write(data)
	_ = cmp.Close()
	data2 := buffer.Bytes()
	if len(data2) >= len(data) {
		return data, ""
	}

	// return the compressed data
	return data2, "FlateDecode"
}
