module gitlab.com/stone.code/pdfwriter

go 1.8

require (
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	golang.org/x/image v0.0.0-20191009234506-e7c1f5e7dbb8
	gonum.org/v1/plot v0.0.0-20191107103940-ca91d9d40d0a
)
