# PDF Writer

Package pdfwriter provides a mid-level generator for PDF documents.  The
goal is to provide an API at the same level of abstraction as PDF documents
themselves.  The document structure can be defined, as well as adding pages
with text, graphics, and annotations.

PDFs are generated during testing, and can be used to explore the capabilities of this package.  These include [TestDraw](https://stone.code.gitlab.io/pdfwriter/TestDraw.pdf), [TestImage](https://stone.code.gitlab.io/pdfwriter/TestImage.pdf), and [TestText](https://stone.code.gitlab.io/pdfwriter/TestText.pdf).

References:

* ISO 32000-1 Document management -- Portable document format -- Part 1: PDF 1.7

## Install

The package can be installed from the command line using the
[go](https://golang.org/cmd/go/) tool.  Beyond what is provided by the standard
library, there are no platform dependencies.

    go get gitlab.com/stone.code/pdfwriter

## Getting Started

Package documentation and examples are on [godoc](https://godoc.org/gitlab.com/stone.code/pdfwriter).

Example use of the package can be found in the file `pdf_test.go`.

## Contribute

Feedback and PRs welcome.  You can also [submit an issue](https://gitlab.com/stone.code/pdfwriter/issues).

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/stone.code/pdfwriter)](https://goreportcard.com/report/gitlab.com/stone.code/pdfwriter)

### Scope

This projects aims to provide an API capable of generating any document that would be supported by the PDF format.  However, the standard for the format is very large, and some features may only be implemented as need arises.

Low-level primitives (such as described in section 3 of the reference) will not be exposed.  These are considered too low-level, and should be mapped to primitives in Go.

## License

BSD (c) Robert Johnstone
