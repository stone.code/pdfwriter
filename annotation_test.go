package pdf

import (
	"testing"
)

func TestLinkAnnotation(t *testing.T) {
	pdf := New(Size{1 * Inch, 1 * Inch})
	pdf.AddPage()
	pdf.Rectangle(12, 12, 12, 12, Fill)
	pdf.AddInternalLink(Rect(12, 12, 24, 24), 1, Point{12, 12})
	pdf.Rectangle(12, 30, 12, 12, Fill)
	pdf.AddInternalLink(Rect(12, 30, 24, 42), 2, Point{12, 12})
	pdf.Rectangle(12, 48, 12, 12, Fill)
	pdf.AddExternalLink(Rect(12, 48, 24, 60), "https://golang.org/")
	pdf.AddPage()
	pdf.SetFillRGB(128, 0, 0)
	pdf.Rectangle(12, 12, 12, 12, Fill)
	pdf.AddInternalLink(Rect(12, 12, 24, 24), 0, Point{12, 12})
	pdf.SetFillRGB(255, 0, 0)
	pdf.Rectangle(12, 30, 12, 12, Fill)
	pdf.AddTextAnnotation(Rect(12, 30, 24, 42), "This is some text in a note.\n\nLorem ipsum...")
	pdf.AddPage()
	pdf.SetFillRGB(0, 128, 0)
	pdf.Rectangle(12, 12, 12, 12, Fill)
	pdf.AddInternalLink(Rect(12, 12, 24, 24), 0, Point{12, 12})

	// Close and write the PDF file.
	writeFile(t, pdf)
}

func TestTextAnnotation(t *testing.T) {
	pdf := New(Size{8.5 * Inch, 11 * Inch})
	pdf.DisableCompression()
	pdf.AddPage()
	pdf.Line(Point{0, (11 - 1) * Inch}, Point{8.5 * Inch, (11 - 1) * Inch})
	mustSetFont(t, pdf, Helvetica, Bold, 12)
	pdf.Text("Title", 72, (11-1)*Inch)
	pdf.AddTextAnnotation(Rect(8.5*Inch/2, 10.5*Inch, 8.5*Inch, 10.5*Inch), "This is some text in a note.\n\nLorem ipsum...")

	// Close and write the PDF file.
	writeFile(t, pdf)
}

func TestFormAnnotations(t *testing.T) {
	pdf := New(Size{8.5 * Inch, 11 * Inch})
	pdf.DisableCompression()
	pdf.AddPage()
	pdf.Line(Point{0, (11 - 1) * Inch}, Point{8.5 * Inch, (11 - 1) * Inch})
	mustSetFont(t, pdf, Helvetica, Bold, 12)
	pdf.Text("Title", 72, (11-1)*Inch)
	pdf.AddTextAnnotation(Rect(8.5*Inch/2, 10.5*Inch, 8.5*Inch, 10.5*Inch), "This is some text in a note.\n\nLorem ipsum...")
	mustSetFont(t, pdf, Dingbats, Normal, 12)
	pdf.AddCheckboxField(Rect(72, 9*Inch, 144, 9.5*Inch), "ok", false)
	pdf.AddCheckboxField(Rect(72, 8*Inch, 144, 9.5*Inch), "nok", true)
	pdf.AddTextField(Rect(1*Inch, 7*Inch, 7.5*Inch, 8.5*Inch), "sometext", "Hello, world!", TextFieldNoOpt, -1)
	pdf.AddTextField(Rect(1*Inch, 6*Inch, 7.5*Inch, 7.5*Inch), "somepass", "Hello, world!", TextFieldPassword, -1)
	pdf.AddSignatureField(Rect(1*Inch, 6*Inch, 7.5*Inch, 6.5*Inch), "somsig")

	// Close and write the PDF file.
	writeFile(t, pdf)
}
