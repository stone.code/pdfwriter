// Package pdf provides a mid-level generator for PDF documents.  The
// goal is to provide an API at the same level of abstraction as PDF documents
// themselves.  The document structure can be defined, as well as adding pages
// with text, graphics, and annotations.
//
// The coordinate system used is based on the native format for PDF documents.
// Although scaling transforms are possible, all distances and dimensions are
// specified in points (1/72 inch).  The origin for the coordinate system is in
// the bottom left corner.
//
// Low-level primitives (such as described in section 3 of the reference) will
// not be exposed.
//
// References
//
// ISO 32000-1 Document management -- Portable document format -- Part 1: PDF 1.7
package pdf
