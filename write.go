package pdf

import (
	"bytes"
	"fmt"
	"image/color"
	"time"
)

func (p *PDF) writeHeader(w *bytes.Buffer) {
	fmt.Fprintf(w, "%%PDF-%d.%d\n", p.version.Major, p.version.Minor)
}

// Converts a color index, represented by a uint8 to match the package image/color,
// to a decimal float string between 0 and 1.
// At most, this add 7 bytes.
func writeColor(s []byte, clr uint8) []byte {
	// Fast path for zero
	if clr == 0 {
		return append(s, '0', ' ')
	}
	if clr == 0xff {
		return append(s, '1', ' ')
	}

	// Convert the uint8 into a higher precision value, maintaining precision
	// but keeping within uint32
	a := uint32(clr) * 3921569
	a /= 100000
	return append(s, '0', '.', '0'+byte(a/1000), '0'+byte((a/100)%10), '0'+byte((a/10)%10), '0'+byte(a%10), ' ')
}

func bookmarkChildCount(tree map[int][]int, index int) int {
	parentIndex := index + 1
	children := tree[parentIndex]
	count := len(children)
	for _, v := range children {
		count += bookmarkChildCount(tree, v)
	}
	return count
}

func (p *PDF) writeAnnotation(w *bytes.Buffer, annot annotation, fieldsOffset int) {
	switch annot.kind {
	case annotationText:
		fmt.Fprintf(w, "<</Type /Annot /Subtype /Text /Rect [%.2f %.2f %.2f %.2f] /Border [0 0 0] /Contents %s>>\n",
			annot.bounds.Min.X, annot.bounds.Min.Y, annot.bounds.Max.X, annot.bounds.Max.Y,
			p.formatString(annot.contents))
	case annotationLink:
		fmt.Fprintf(w, "<</Type /Annot /Subtype /Link /Rect [%.2f %.2f %.2f %.2f] /Border [0 0 0] /Dest [%d 0 R /XYZ %.2f %.2f null] >>\n",
			annot.bounds.Min.X, annot.bounds.Min.Y, annot.bounds.Max.X, annot.bounds.Max.Y,
			3+2*annot.pageIndex, annot.target.X, annot.target.Y)
	case annotationLinkURL:
		fmt.Fprintf(w, "<</Type /Annot /Subtype /Link /Rect [%.2f %.2f %.2f %.2f] /Border [0 0 0] /A <</S /URI /URI %s>> >>\n",
			annot.bounds.Min.X, annot.bounds.Min.Y, annot.bounds.Max.X, annot.bounds.Max.Y,
			p.formatString(annot.url))
	case annotationWidget:
		fmt.Fprintf(w, "%d 0 R ", fieldsOffset+annot.field*2)
	}
}

func (p *PDF) writeBookmarks(w *bytes.Buffer) int {
	if len(p.bookmarks) == 0 {
		return 0
	}

	// To write out the bookmarks, there are many links up and down the tree
	// required by the standard (section 8.2.2).  The flat list constructed at
	// this point does not have enough information for the queries that we need,
	// so we need to create a map of parents to their children.
	tree := map[int][]int{}
	for i, v := range p.bookmarks {
		parentIndex := v.parentIndex
		tree[parentIndex] = append(tree[parentIndex], i)
	}

	first, last := p.writeBookmarksBranch(w, tree, -1, 0)

	ndx := p.newobj(w)
	fmt.Fprintf(w, "<</Type /Outlines /First %d 0 R /Last %d 0 R>>\n", first, last)
	p.endobj(w)

	return ndx
}

func (p *PDF) writeBookmarksBranch(w *bytes.Buffer, tree map[int][]int, index int, parent int) (int, int) {
	first, last := 0, 0

	items := tree[index+1]
	if len(items) == 0 {
		return 0, 0
	}

	for i, v := range items {
		childCount := bookmarkChildCount(tree, v)
		bookmark := p.bookmarks[v]

		ndxPredicted := len(p.objects) + 1 + childCount
		childFirst, childLast := p.writeBookmarksBranch(w, tree, v, ndxPredicted)

		ndx := p.newobj(w)
		if ndx != ndxPredicted {
			panic("implementation error")
		}
		fmt.Fprintf(w, "<</Title %s /Dest [%d 0 R /XYZ %.2f %.2f null] /Count %d",
			p.formatString(bookmark.text), 3+2*bookmark.pageIndex, bookmark.target.X, bookmark.target.Y, -childCount,
		)
		if first == 0 {
			first = ndx
		} else {
			fmt.Fprintf(w, " /Prev %d 0 R", last)
		}
		last = ndx
		if i < len(items)-1 {
			fmt.Fprintf(w, " /Next %d 0 R", ndx+1+bookmarkChildCount(tree, items[i+1]))
		}
		if parent > 0 {
			fmt.Fprintf(w, " /Parent %d 0 R", parent)
		}
		if childFirst != 0 {
			fmt.Fprintf(w, " /First %d 0 R /Last %d 0 R", childFirst, childLast)
		}
		if !p.version.LT(1, 4) {
			if bookmark.clr.R != 0 || bookmark.clr.G != 0 || bookmark.clr.B != 0 {
				fmt.Fprintf(w, " /C [%.2f %.2f %.2f]", float32(bookmark.clr.R)/0xff,
					float32(bookmark.clr.G)/0xff, float32(bookmark.clr.B)/0xff)
			}
			if bookmark.style != 0 {
				s := uint(0)
				if (bookmark.style & Bold) != 0 {
					s |= 2
				}
				if (bookmark.style & Italic) != 0 {
					s |= 1
				}
				fmt.Fprintf(w, " /F %d", s)
			}
		}
		w.WriteString(">>\n")

		p.endobj(w)
	}

	return first, last
}

func (p *PDF) writeCatalog(w *bytes.Buffer, booksmarksOffset, fieldsOffset int) {
	p.newobj(w)
	w.WriteString("<</Type /Catalog /Pages 1 0 R\n")

	// TODO:  We can add support for specifying the open action here.
	//		Full page ->  /OpenAction [3 0 R /Fit]
	//		Full width -> /OpenAction [3 0 R /FitH null]
	//		->				/OpenAction [3 0 R /XYZ null null 1]

	// TODO:  We can add support for specifying the layout mode
	//		/PageLayout /SinglePage
	//		/PageLayout /OneColumn
	//		/PageLayout /TwoColumnLeft
	//		/PageLayout /TwoColumnRight

	// Bookmarks
	if len(p.bookmarks) > 0 {
		fmt.Fprintf(w, "/Outlines %d 0 R /PageMode /UseOutlines\n", booksmarksOffset)
	}

	// Forms
	if len(p.fields) > 0 {
		fmt.Fprintf(w, "/AcroForm <<\n")
		fmt.Fprintf(w, "/Fields [ ")
		for i := range p.fields {
			fmt.Fprintf(w, "%d ", fieldsOffset+2*i)
		}
		fmt.Fprintf(w, "]\n")
		fmt.Fprintf(w, ">>\n")
	}

	w.WriteString(">>\n")
	p.endobj(w)
}

func (p *PDF) writeEncrypt(w *bytes.Buffer) int {
	// If no encryption has been set, then there is no encrypt dictionary
	// required.
	if p.encrypt.revision == 0 {
		return 0
	}

	objectNdx := p.newobj(w)
	fmt.Fprintf(w, "<</Filter /Standard /V 1 /R %d /O (%s) /U (%s) /P %d>>\n",
		p.encrypt.revision,
		escape(string(p.encrypt.encryptionO[:])),
		escape(string(p.encrypt.encryptionU[:])),
		uint32(p.encrypt.permissions)|0xFFFF0000,
	)
	p.endobj(w)

	return objectNdx
}

func (p *PDF) writeFields(w *bytes.Buffer) int {
	retndx := 0

	for i, v := range p.fields {
		ndx := p.newobj(w)
		p.writeField(w, ndx, v)
		p.endobj(w)
		if i == 0 {
			retndx = ndx
		}

		ndx = p.newobj(w)
		p.writeFieldResource(w, ndx, v)
		p.endobj(w)
	}

	return retndx
}

func (p *PDF) writeField(w *bytes.Buffer, ndx int, f field) {
	switch f.kind {
	case fieldCheckbox:
		fmt.Fprintf(w, "<< /Type /Annot /Subtype /Widget /Rect [%.2f %.2f %.2f %.2f] /FT /Btn /T %s /V /Yes /AS /Yes /AP << /N << /Yes %d 0 R >> >> >>\n",
			f.bounds.Min.X, f.bounds.Min.Y, f.bounds.Max.X, f.bounds.Max.Y,
			p.formatString(f.name), ndx+1)
	case fieldText:
		fmt.Fprintf(w, "<< /Type /Annot /Subtype /Widget /Rect [%.2f %.2f %.2f %.2f] /FT /Tx /T %s /V %s /AS /Yes /AP << /N %d 0 R >> ",
			f.bounds.Min.X, f.bounds.Min.Y, f.bounds.Max.X, f.bounds.Max.Y,
			p.formatString(f.name), p.formatString(f.value), ndx+1)
		if f.flags != 0 {
			fmt.Fprintf(w, "/Ff %d ", f.flags)
		}
		if f.maxlen > 0 {
			fmt.Fprintf(w, "/MaxLen %d ", f.maxlen)
		}
		fmt.Fprintf(w, ">>\n")
	case fieldSignature:
		fmt.Fprintf(w, "<< /Type /Annot /Subtype /Widget /Rect [%.2f %.2f %.2f %.2f] /FT /Sig /T %s >>\n",
			f.bounds.Min.X, f.bounds.Min.Y, f.bounds.Max.X, f.bounds.Max.Y,
			p.formatString(f.name))
	default:
		panic("unreachable")
	}
}

func (p *PDF) writeFieldResource(w *bytes.Buffer, ndx int, f field) {
	const Btn = "q 0 0 1 rg BT /F2 12 Tf 144 144 Td (8) Tj ET Q\n"
	const Tx = "/Tx BMC BT 0 0 1 rg /F2 12 Tf 1 0 0 1 100 100 Tm 0 0 Td (text) Tj ET EMC\n"

	stream := Btn
	if f.kind == fieldText || f.kind == fieldSignature {
		stream = Tx
	}

	data, filter := compress([]byte(stream), p.disableCompression)
	if filter != "" {
		fmt.Fprintf(w, "<</Resources 2 0 R /Filter /%s /Length %d>>", filter, len(data))
		p.putstream(w, data)
	} else {
		fmt.Fprintf(w, "<</Resources 2 0 R /Length %d>>", len(data))
		p.putstream(w, data)
	}
}

func (p *PDF) writeFonts(w *bytes.Buffer) int {
	// Write out font resources.  Depending on the font type, a different
	// number of resources might be required.  To allow the font catalog
	// to recover the object indices, we need the actual dictionary with
	// the font information to be consecutive.
	for _, v := range p.fonts {
		v.writeResources(p, w)
	}

	// Write out the dictionaries.
	objectsIndex := len(p.objects) + 1
	for _, v := range p.fonts {
		p.newobj(w)
		v.writeDict(w)
		p.endobj(w)
	}

	// Return the first index of the font dictionaries for
	return objectsIndex
}

func (p *PDF) writeGradients(w *bytes.Buffer) int {
	objectsIndex := len(p.objects) + 1
	for _, v := range p.gradients {
		v.write(p, w)
	}
	return objectsIndex
}

func (p *PDF) writeGStates(w *bytes.Buffer) int {
	objectsIndex := len(p.objects) + 1
	for _, v := range p.gstates {
		p.newobj(w)
		fmt.Fprintf(w, "<</Type /ExtGState /BM /%s /ca %.3f /CA %.3f>>\n", v.BM.postscript(), v.ca, v.CA)
		p.endobj(w)
	}
	return objectsIndex
}

func (p *PDF) writeImages(w *bytes.Buffer) int {
	objectsIndex := len(p.objects) + 1

	for _, v := range p.images {
		// Create an object for this image
		p.newobj(w)

		// Write out dictionary describing the image
		fmt.Fprintf(w, "<</Type /XObject /Subtype /Image /Width %d /Height %d ", v.config.Width, v.config.Height)
		switch v.config.ColorModel {
		case color.GrayModel:
			w.WriteString("/ColorSpace /DeviceGray /BitsPerComponent 8 ")
		case color.Gray16Model:
			w.WriteString("/ColorSpace /DeviceGray /BitsPerComponent 16 ")
		case color.RGBAModel:
			w.WriteString("/ColorSpace /DeviceRGB /BitsPerComponent 8 ")
		case color.YCbCrModel:
			w.WriteString("/ColorSpace /DeviceRGB /BitsPerComponent 8 ")
		case color.CMYKModel:
			w.WriteString("/ColorSpace /DeviceCMYK /Decode [1 0 1 0 1 0 1 0] /BitsPerComponent 8 ")
		default:
			panic("Not implemented.")
		}
		if v.decodeParams != "" {
			fmt.Fprintf(w, "/DecodeParms <<%s>> ", v.decodeParams)
		}
		if v.filter != "" {
			fmt.Fprintf(w, "/Filter /%s ", v.filter)
		}
		if v.smask != 0 {
			fmt.Fprintf(w, "/SMask %d 0 R ", objectsIndex+v.smask-1)
		}
		fmt.Fprintf(w, "/Length %d>>\n", len(v.data))

		// Write out stream with the image data
		p.putstream(w, v.data)

		// Close the object
		p.endobj(w)
	}

	return objectsIndex
}

func (p *PDF) writeInfo(w *bytes.Buffer) int {
	objectNdx := p.newobj(w)

	fmt.Fprintf(w, "<</Producer %s ", p.formatString("bitbucket.org/rj/pdf"))
	if len(p.metadata.title) > 0 {
		fmt.Fprintf(w, "/Title %s ", p.formatString(p.metadata.title))
	}
	if len(p.metadata.subject) > 0 {
		fmt.Fprintf(w, "/Subject %s ", p.formatString(p.metadata.subject))
	}
	if len(p.metadata.author) > 0 {
		fmt.Fprintf(w, "/Author %s ", p.formatString(p.metadata.author))
	}
	if len(p.metadata.keywords) > 0 {
		fmt.Fprintf(w, "/Keywords %s ", p.formatString(p.metadata.keywords))
	}
	if len(p.metadata.creator) > 0 {
		fmt.Fprintf(w, "/Creator %s ", p.formatString(p.metadata.creator))
	}
	if p.metadata.creation.IsZero() {
		tm := time.Now()
		fmt.Fprintf(w, "/CreationDate (D:%s) ", tm.Format("20060102150405"))
	} else {
		fmt.Fprintf(w, "/CreationDate (D:%s) ", p.metadata.creation.Format("20060102150405"))
	}

	w.WriteString(">>\n")
	p.endobj(w)

	return objectNdx
}

func (p *PDF) writePages(w *bytes.Buffer, fieldsOffset int) {
	// Write out the objects for all of the pages
	for n, page := range p.pages {
		// Page
		objectNdx := p.newobj(w)
		w.WriteString("<</Type /Page /Parent 1 0 R ")

		if pageSize, ok := p.pageSizes[n]; ok {
			fmt.Fprintf(w, "/MediaBox [0 0 %.2f %.2f]\n", pageSize.Width, pageSize.Height)
		}
		w.WriteString("/Resources 2 0 R\n")

		if annots := page.annots; len(annots) > 0 {
			w.WriteString("/Annots [\n")
			for _, annot := range annots {
				p.writeAnnotation(w, annot, fieldsOffset)
			}
			w.WriteString("]\n")
		}

		if p.version.GT(1, 3) {
			w.WriteString("/Group <</Type /Group /S /Transparency /CS /DeviceRGB>>\n")
		}
		fmt.Fprintf(w, "/Contents %d 0 R>>\n", objectNdx+1)
		p.endobj(w)

		// Page content
		p.newobj(w)
		data, filter := compress(page.Bytes(), p.disableCompression)
		if filter != "" {
			fmt.Fprintf(w, "<</Filter /%s /Length %d>>", filter, len(data))
			p.putstream(w, data)
		} else {
			fmt.Fprintf(w, "<</Length %d>>", page.Len())
			p.putstream(w, page.Bytes())
		}
		p.endobj(w)
	}

	// Pages root
	p.objects[0] = w.Len()
	w.WriteString("1 0 obj\n")
	w.WriteString("<</Type /Pages /Kids [")
	for i := range p.pages {
		fmt.Fprintf(w, "%d 0 R ", 3+2*i)
	}
	w.WriteString("]\n")
	fmt.Fprintf(w, "/Count %d ", len(p.pages))
	fmt.Fprintf(w, "/MediaBox [0 0 %.2f %.2f]\n", p.defPageSize.Width, p.defPageSize.Height)
	w.WriteString(">>\n")
	p.endobj(w)
}

func (p *PDF) writeResources(w *bytes.Buffer) {
	fontObjectsIndex := p.writeFonts(w)
	imageObjectsIndex := p.writeImages(w)
	gradientObjectsIndex := p.writeGradients(w)
	gstatesObjectsIndex := p.writeGStates(w)

	// 	Resource dictionary
	p.objects[1] = w.Len()
	w.WriteString("2 0 obj\n")
	w.WriteString("<</ProcSet [/PDF /Text /ImageB /ImageC /ImageI]\n")
	if len(p.fonts) > 0 {
		w.WriteString("/Font <<\n")
		for i := range p.fonts {
			fmt.Fprintf(w, "/F%d %d 0 R\n", i+1, fontObjectsIndex+i)
		}
		w.WriteString(">>\n")
	}
	if len(p.gradients) > 0 {
		w.WriteString("/Shading <<\n")
		for i := range p.gradients {
			fmt.Fprintf(w, "/G%d %d 0 R\n", i+1, gradientObjectsIndex+2*i)
		}
		w.WriteString(">>\n")
	}
	if len(p.images) > 0 {
		w.WriteString("/XObject <<\n")
		for i := range p.images {
			fmt.Fprintf(w, "/I%d %d 0 R\n", i+1, imageObjectsIndex+i)
		}
		w.WriteString(">>\n")
	}
	if len(p.gstates) > 0 {
		w.WriteString("/ExtGState <<\n")
		for i := range p.gstates {
			fmt.Fprintf(w, "/G%d %d 0 R\n", i+1, gstatesObjectsIndex+i)
		}
		w.WriteString(">>\n")
	}
	w.WriteString(">>\n")
	w.WriteString("endobj\n")
}

func (p *PDF) writeTrailer(w *bytes.Buffer, encryptNdx int) {
	ndx := len(p.objects)

	w.WriteString("trailer\n")
	w.WriteString("<<\n")
	fmt.Fprintf(w, "/Size %d\n", ndx+1)
	fmt.Fprintf(w, "/Root %d 0 R\n", ndx)
	fmt.Fprintf(w, "/Info %d 0 R\n", ndx-1)
	if encryptNdx != 0 {
		fmt.Fprintf(w, "/Encrypt %d 0 R /ID [()()]\n", encryptNdx)
	}
	w.WriteString(">>\n")
}

func (p *PDF) writeXref(w *bytes.Buffer) int {
	// See section 3.4.3 on the cross-reference table.
	offset := w.Len()
	w.WriteString("xref\n")

	// Each section starts with two numbers.
	// - the index of the first entry
	// - the number of entries.
	// For this file, we only have one section
	fmt.Fprintf(w, "0 %d\n", len(p.objects)+1)

	// According to the standard, the first item (index 0) is always free.
	// It has a required offset and generation.  Note that the record
	// must be exactly 20 bytes long.
	w.WriteString("0000000000 65535 f \n")

	// Write out the entries.  Each entry consists of the offset and
	// a generation.  Each record must be exactly 20 bytes long.
	for _, v := range p.objects {
		fmt.Fprintf(w, "%010d 00000 n \n", v)
	}

	return offset
}
