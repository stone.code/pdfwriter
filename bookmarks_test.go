package pdf

import (
	"image/color"
	"testing"
)

func TestAddBookmark(t *testing.T) {
	pdf := New(Letter)
	pdf.DisableCompression()
	pdf.AddPage()
	mustSetFont(t, pdf, Times, Normal, 12)
	pdf.Text("Chapter 1", 72, Letter.Height-72)
	pdf.Text("Chapter 1.1", 72, Letter.Height-72-18)
	pdf.Text("Chapter 1.1.1", 72, Letter.Height-72-2*18)
	pdf.Text("Chapter 1.1.2", 72, Letter.Height-72-3*18)
	pdf.Text("Chapter 1.1.3", 72, Letter.Height-72-4*18)
	pdf.Text("Chapter 1.2", 72, Letter.Height-72-5*18)
	pdf.Text("Chapter 1.3", 72, Letter.Height-72-6*18)
	ndx := pdf.AddBookmark("Chapter 1", Point{72, Letter.Height - 72 + 12})
	ndx2 := pdf.AddBookmarkChild("Chapter 1.1", Point{72, Letter.Height - 72 + 12 - 18}, ndx)
	pdf.AddBookmarkChild("Chapter 1.1.1", Point{72, Letter.Height - 72 + 12 - 2*18}, ndx2)
	pdf.AddBookmarkChild("Chapter 1.1.2", Point{72, Letter.Height - 72 + 12 - 3*18}, ndx2)
	pdf.AddBookmarkChild("Chapter 1.1.3", Point{72, Letter.Height - 72 + 12 - 4*18}, ndx2)
	pdf.AddBookmarkChild("Chapter 1.2", Point{72, Letter.Height - 72 + 12 - 5*18}, ndx)
	pdf.AddBookmarkChild("Chapter 1.3", Point{72, Letter.Height - 72 + 12 - 6*18}, ndx)
	ndx = pdf.AddBookmark("Chapter 1 (dup)", Point{72, Letter.Height - 72 + 12})
	pdf.AddBookmarkChild("Chapter 1.1", Point{72, Letter.Height - 72 + 12 - 18}, ndx)
	pdf.AddPage()
	mustSetFont(t, pdf, Times, Normal, 12)
	pdf.Text("Chapter 2", 72, Letter.Height-72)
	pdf.AddBookmark("Chapter 2", Point{72, Letter.Height - 72 + 12})

	// Close and write the PDF file.
	writeFile(t, pdf)
}

func TestAddBookmark2(t *testing.T) {
	pdf := New(Letter)
	pdf.SetMinimumVersion(Version{1, 4})
	pdf.DisableCompression()
	pdf.AddPage()
	mustSetFont(t, pdf, Times, Normal, 12)
	pdf.Text("Chapter 1", 72, Letter.Height-72)
	pdf.Text("Chapter 1.1", 72, Letter.Height-72-18)
	pdf.Text("Chapter 1.1.1", 72, Letter.Height-72-2*18)
	pdf.Text("Chapter 1.1.2", 72, Letter.Height-72-3*18)
	pdf.Text("Chapter 1.1.3", 72, Letter.Height-72-4*18)
	pdf.Text("Chapter 1.2", 72, Letter.Height-72-5*18)
	pdf.Text("Chapter 1.3", 72, Letter.Height-72-6*18)
	ndx := pdf.AddBookmark("Chapter 1", Point{72, Letter.Height - 72 + 12})
	pdf.SetBookmarkStyle(ndx, color.RGBA{}, Bold)
	ndx2 := pdf.AddBookmarkChild("Chapter 1.1", Point{72, Letter.Height - 72 + 12 - 18}, ndx)
	pdf.SetBookmarkStyle(ndx2, color.RGBA{0, 0, 255, 255}, 0)
	pdf.AddBookmarkChild("Chapter 1.1.1", Point{72, Letter.Height - 72 + 12 - 2*18}, ndx2)
	pdf.AddBookmarkChild("Chapter 1.1.2", Point{72, Letter.Height - 72 + 12 - 3*18}, ndx2)
	pdf.AddBookmarkChild("Chapter 1.1.3", Point{72, Letter.Height - 72 + 12 - 4*18}, ndx2)
	pdf.AddBookmarkChild("Chapter 1.2", Point{72, Letter.Height - 72 + 12 - 5*18}, ndx)
	pdf.AddBookmarkChild("Chapter 1.3", Point{72, Letter.Height - 72 + 12 - 6*18}, ndx)
	ndx = pdf.AddBookmark("Chapter 1 (dup)", Point{72, Letter.Height - 72 + 12})
	pdf.AddBookmarkChild("Chapter 1.1", Point{72, Letter.Height - 72 + 12 - 18}, ndx)
	pdf.AddPage()
	mustSetFont(t, pdf, Times, Normal, 12)
	pdf.Text("Chapter 2", 72, Letter.Height-72)
	ndx = pdf.AddBookmark("Chapter 2", Point{72, Letter.Height - 72 + 12})
	pdf.SetBookmarkStyle(ndx, color.RGBA{}, Bold)

	// Close and write the PDF file.
	writeFile(t, pdf)
}
