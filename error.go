package pdf

import (
	"errors"
)

var (
	// ErrRequiredVersion is returned when the version specified for the
	// document is not sufficient to support all the features used
	ErrRequiredVersion = errors.New("version is insufficient to support all the features used")
)
