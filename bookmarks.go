package pdf

import (
	"image/color"
)

// bookmarkResource describes an outline item as described in section 8.2.2 of
// the PDF reference.
type bookmarkResource struct {
	text        string
	pageIndex   int
	target      Point
	parentIndex int
	clr         color.RGBA
	style       FontStyle
}

// AddBookmark creates a top-level bookmark for the PDF document.  When created, the
// bookmark will show up in the table of contents, and point to the current
// page.
//
// This function will panic if no page is currently open.  See the method CurrentPage.
func (p *PDF) AddBookmark(text string, target Point) int {
	ndx := len(p.bookmarks)
	p.bookmarks = append(p.bookmarks, bookmarkResource{
		text:      text,
		pageIndex: p.currentPage.index,
		target:    target,
	})
	return ndx
}

// SetBookmarkStyle adds styling information to a bookmark.  However, the style
// information will only be written if the version is set to 1.4 or greater.
// See the method SetMinimumVersion.
//
// Although the parameter colour is an RGBA, alpha is not supported in bookmarks.
//
// Underlining is not supported in bookmarks.
func (p *PDF) SetBookmarkStyle(bookmarkIndex int, clr color.RGBA, style FontStyle) {
	p.bookmarks[bookmarkIndex].clr = clr
	p.bookmarks[bookmarkIndex].style = style
}

// AddBookmarkChild creates a bookmark for the PDF document.  When created, the
// bookmark will show up in the table of contents, and point to the current
// page.  The bookmark will appear as a child of the bookmark specified by
// parentIndex.
//
// This function will panic if no page is currently open.  See the method CurrentPage.
func (p *PDF) AddBookmarkChild(text string, target Point, parentIndex int) int {
	// Note, the parentIndex is stored with a 1-based count, not a 0-based
	// count.  This is done so that zero can be used to indicate no parent.

	// Make sure that the bookmark exists
	_ = p.bookmarks[parentIndex]

	// Add the bookmarks
	ndx := len(p.bookmarks)
	p.bookmarks = append(p.bookmarks, bookmarkResource{
		text:        text,
		pageIndex:   p.currentPage.index,
		target:      target,
		parentIndex: parentIndex + 1,
	})
	return ndx
}
