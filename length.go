package pdf

import (
	"strconv"
)

// A PT is a representation of length stored in postscript points.
type PT float64

// Commonly used units of measure.
const (
	Pt   PT = 1
	Inch PT = 72        // Convert inches to points.
	Cm   PT = 72 / 2.54 // Convert centimeters to points.
	Mm   PT = 72 / 25.4 // Convert millimeters to points.
	Pica PT = 72 / 6    // Convert postscript pica to points.
)

// Mm converts the length to millimeters, and returns the value as an undimensioned float.
func (pt PT) Mm() float64 {
	return float64(pt) * (25.4 / 72)
}

// Cm converts the length to centimeters, and returns the value as an undimensioned float.
func (pt PT) Cm() float64 {
	return float64(pt) * (2.54 / 72)
}

// Inches converts the length to inches, and returns the value as an undimensioned float.
func (pt PT) Inches() float64 {
	return float64(pt) / 72
}

// Pica converts the length to pica, and returns the value as an undimensioned float.
func (pt PT) Pica() float64 {
	return float64(pt) * (6.0 / 72)
}

// Points converts the length to points (1/72"), and returns the value as an undimensioned float.
func (pt PT) Points() float64 {
	return float64(pt)
}

// Scale scales the length by ratio of num:den.
func (pt PT) Scale(num, den int) PT {
	return PT(pt) * PT(num) / PT(den)
}

// A Point is an X, Y coordinate pair.
type Point struct {
	X, Y PT
}

// String returns a string representation of p like "(3,4)".
func (p Point) String() string {
	return "(" + strconv.FormatFloat(float64(p.X), 'g', -1, 64) + "," + strconv.FormatFloat(float64(p.Y), 'g', -1, 64) + ")"
}

// Add returns the vector p+q.
func (p Point) Add(q Point) Point {
	return Point{p.X + q.X, p.Y + q.Y}
}

// Sub returns the vector p-q.
func (p Point) Sub(q Point) Point {
	return Point{p.X - q.X, p.Y - q.Y}
}

// Mul returns the vector p*k.
func (p Point) Mul(k float64) Point {
	return Point{p.X * PT(k), p.Y * PT(k)}
}

// A Rectangle contains the points with Min.X <= X < Max.X, Min.Y <= Y < Max.Y.
// It is well-formed if Min.X <= Max.X and likewise for Y.
// Points are always well-formed.
// A rectangle's methods always return well-formed outputs for well-formed inputs.
type Rectangle struct {
	Min, Max Point
}

// Canon returns the canonical version of r. The returned rectangle has minimum
// and maximum coordinates swapped if necessary so that it is well-formed.
func (r Rectangle) Canon() Rectangle {
	if r.Max.X < r.Min.X {
		r.Min.X, r.Max.X = r.Max.X, r.Min.X
	}
	if r.Max.Y < r.Min.Y {
		r.Min.Y, r.Max.Y = r.Max.Y, r.Min.Y
	}
	return r
}

// Rect is shorthand for Rectangle{Pt(x0, y0), Pt(x1, y1)}. The returned
// rectangle has minimum and maximum coordinates swapped if necessary so that
// it is well-formed.
func Rect(x0, y0, x1, y1 PT) Rectangle {
	if x0 > x1 {
		x0, x1 = x1, x0
	}
	if y0 > y1 {
		y0, y1 = y1, y0
	}
	return Rectangle{Point{x0, y0}, Point{x1, y1}}
}

// String returns a string representation of r like "(3,4)-(6,5)".
func (r Rectangle) String() string {
	return r.Min.String() + "-" + r.Max.String()
}

// Dx returns r's width.
func (r Rectangle) Dx() PT {
	return r.Max.X - r.Min.X
}

// Dy returns r's height.
func (r Rectangle) Dy() PT {
	return r.Max.Y - r.Min.Y
}

// Size returns r's width and height.
func (r Rectangle) Size() Point {
	return Point{
		r.Max.X - r.Min.X,
		r.Max.Y - r.Min.Y,
	}
}
