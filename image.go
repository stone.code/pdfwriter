package pdf

import (
	"bytes"
	"image"
	"image/color"
	"image/draw"
)

type imageResource struct {
	data         []byte
	config       image.Config
	filter       string
	decodeParams string
	smask        int
}

// AddImage creates a resource in the PDF document with the image data.
// The function returns an index, which can be used with the method Image
// to draw the picture onto a page.
//
// Note, that images of type image.Gray, image.Gray16, and image.RGBA are
// supported by copying the pixel data directly.  Other images type will
// require a potentially expensive conversion to RGBA before the image
// data will be written to the PDF document.
func (p *PDF) AddImage(img image.Image) int {
	// Depending on the image, different encodings will
	// be necessary.
	if gray, ok := img.(*image.Gray); ok {
		return p.addImageGray(gray)
	}
	if gray, ok := img.(*image.Gray16); ok {
		return p.addImageGray16(gray)
	}
	if rgba, ok := img.(*image.RGBA); ok {
		return p.addImageRGBA(rgba)
	}

	// Need to convert the image to a known format.  RGBA.
	bounds := img.Bounds()
	rgba := image.NewRGBA(bounds)
	draw.Draw(rgba, bounds, img, bounds.Min, draw.Src)
	return p.addImageRGBA(rgba)
}

func (p *PDF) addImageGray(img *image.Gray) int {
	width := img.Rect.Dx()
	height := img.Rect.Dy()

	buffer := bytes.NewBuffer(nil)
	buffer.Grow(width * height)

	for y := img.Rect.Min.Y; y < img.Rect.Max.Y; y++ {
		pos := (y-img.Rect.Min.Y)*img.Stride + img.Rect.Min.X
		buffer.Write(img.Pix[pos : pos+width])
	}

	// Optionally compress the image data
	data, filter := compress(buffer.Bytes(), p.disableCompression)

	// Add the image resource to the PDF
	config := image.Config{ColorModel: color.GrayModel, Width: width, Height: height}
	p.images = append(p.images, imageResource{
		config: config,
		data:   data,
		filter: filter,
	})

	return len(p.images)
}

func (p *PDF) addImageGray16(img *image.Gray16) int {
	width := img.Rect.Dx()
	height := img.Rect.Dy()

	buffer := bytes.NewBuffer(nil)
	buffer.Grow(width * height * 2)

	for y := img.Rect.Min.Y; y < img.Rect.Max.Y; y++ {
		pos := (y-img.Rect.Min.Y)*img.Stride + img.Rect.Min.X
		buffer.Write(img.Pix[pos : pos+width*2])
	}

	// Optionally compress the image data
	data, filter := compress(buffer.Bytes(), p.disableCompression)

	// Add the image resource to the PDF
	config := image.Config{ColorModel: color.Gray16Model, Width: width, Height: height}
	p.images = append(p.images, imageResource{
		config: config,
		data:   data,
		filter: filter,
	})

	return len(p.images)
}

func (p *PDF) addImageRGBA(img *image.RGBA) int {
	width := img.Rect.Dx()
	height := img.Rect.Dy()

	buffer := bytes.NewBuffer(nil)
	buffer.Grow(width * height * 3)
	alpha := bytes.NewBuffer(nil)
	alpha.Grow(width * height)
	alphaUsed := byte(0)

	for y := img.Rect.Min.Y; y < img.Rect.Max.Y; y++ {
		ypos := (y-img.Rect.Min.Y)*img.Stride + img.Rect.Min.X*4
		for x := img.Rect.Min.X; x < img.Rect.Max.X; x++ {
			pos := ypos + (x-img.Rect.Min.X)*4
			buffer.Write(img.Pix[pos : pos+3])
			a := img.Pix[pos+3]
			alpha.WriteByte(a)
			alphaUsed |= a ^ 0xFF
		}
	}

	// Do we need to create a smask?
	smask := 0
	if alphaUsed != 0 {
		// Create an image resource with the 8-bit gray scale used for the
		// alpha channel of the image.
		data, filter := compress(alpha.Bytes(), p.disableCompression)

		config := image.Config{ColorModel: color.GrayModel, Width: width, Height: height}
		p.images = append(p.images, imageResource{
			config: config,
			data:   data,
			filter: filter,
		})

		smask = len(p.images)
	}

	// Optionally compress the image data
	data, filter := compress(buffer.Bytes(), p.disableCompression)

	// Add the image resource to the PDF
	config := image.Config{ColorModel: color.RGBAModel, Width: width, Height: height}
	p.images = append(p.images, imageResource{
		config: config,
		data:   data,
		filter: filter,
		smask:  smask,
	})

	return len(p.images)
}
