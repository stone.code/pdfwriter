package pdf

import (
	"strconv"
)

// Version represents the version of a PDF document.
type Version struct {
	Major uint8
	Minor uint8
}

// String returns a string representation of the PDF document version.
func (v Version) String() string {
	return strconv.FormatUint(uint64(v.Major), 10) + "." + strconv.FormatUint(uint64(v.Minor), 10)
}

// LT returns whether the version is less than the version specified by the parameters.
func (v Version) LT(major uint8, minor uint8) bool {
	return v.Major < major || (v.Major == major && v.Minor < minor)
}

// EQ returns whether the version is the same as the version specified by the parameters.
func (v Version) EQ(major uint8, minor uint8) bool {
	return v.Major == major && v.Minor == minor
}

// GT returns whether the version is greater than the version specified by the parameters.
func (v Version) GT(major uint8, minor uint8) bool {
	return v.Major > major || (v.Major == major && v.Minor > minor)
}
