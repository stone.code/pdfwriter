package pdf

import (
	"fmt"
	"strings"
)

// Escape special characters in strings.
func escape(s string) string {
	// Need to escape control characters from the string
	s = strings.Replace(s, "\\", "\\\\", -1)
	s = strings.Replace(s, "(", "\\(", -1)
	s = strings.Replace(s, ")", "\\)", -1)
	s = strings.Replace(s, "\r", "\\r", -1)
	return s
}

func isASCII(s string) bool {
	for _, v := range s {
		if v >= 0x80 {
			return false
		}
	}
	return true
}

// ToUTF16 converts a string in UTF-8 to UTF-16BE with a BOM.
func toUTF16(s string) []byte {
	// Allocate a buffer for the UTF-16.  Barring any surrogate pairs, the
	// buffer should be the correct length.
	ret := make([]byte, 0, len(s)*2+2)
	// Insert the BOM for the encoding.
	ret = append(ret, 0xFE, 0xFF)

	// Encode the runes in the string
	for _, v := range s {
		if v < 0x10000 {
			// These codepoints are permanently reserved, and should not appear.
			if v >= 0xD800 {
				panic("corrupted UTF-8 string")
			}
			ret = append(ret, byte(v>>8), byte(v))
		} else {
			v2 := uint16(v)
			c1 := (v2 >> 10) | 0xD800
			c2 := (v2 & 0x3F) | 0xDC00
			ret = append(ret, byte(c1>>8), byte(c1), byte(c2>>8), byte(c2))
		}
	}

	// Final buffer
	return ret
}

// FormatString encodes a UTF-8 string for output into the document stream.
// See section 3.8.1 of the PDF reference.  Valid encodings are the
// PDFDocEncoding or UTF-16.
func (p *PDF) formatString(s string) string {
	// If the text is ASCII, we can return it directly without any problem.
	if isASCII(s) {
		// do nothing
	} else if data, ok := PDFDocEncoding.encodeFromUTF8(s); ok {
		// Try to convert the string to the PDF standard encoding
		s = string(data)
	} else {
		// We need to convert the string to UTF-16
		s = string(toUTF16(s))
	}

	// Do we need to encode the text?
	if p.encrypt.revision > 0 {
		data := []byte(s)
		p.encryptData(data, len(p.objects))
		s = string(data)
	}

	return fmt.Sprintf("(%s)", escape(s))
}
