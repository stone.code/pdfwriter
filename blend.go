package pdf

import (
	"fmt"
)

// BlendMode selects the blend function used when composing objects using
// transparency.  For most use cases, the value BlendNormal should be used.
// For details on the other blend modes, refer to section 7.2.4 of the
// reference.
type BlendMode uint8

// All available blend modes supported by PDF documents.
const (
	BlendNormal BlendMode = iota
	BlendMultiply
	BlendScreen
	BlendOverlay
	BlendDarken
	BlendLighten
	BlendColorDodge
	BlendColorBurn
	BlendHardLight
	BlendSoftLight
	BlendDifference
	BlendExclusion
	BlendHue
	BlendSaturation
	BlendColor
	BlendLuminosity
)

func (bm BlendMode) postscript() string {
	switch bm {
	case BlendNormal:
		return "Normal"
	case BlendMultiply:
		return "Multiply"
	case BlendScreen:
		return "Screen"
	case BlendOverlay:
		return "Overlay"
	case BlendDarken:
		return "Darken"
	case BlendLighten:
		return "Lighten"
	case BlendColorDodge:
		return "ColorDodge"
	case BlendColorBurn:
		return "ColorBurn"
	case BlendHardLight:
		return "HardLight"
	case BlendSoftLight:
		return "SoftLight"
	case BlendDifference:
		return "Difference"
	case BlendExclusion:
		return "Exclusion"
	case BlendHue:
		return "Hue"
	case BlendSaturation:
		return "Saturation"
	case BlendColor:
		return "Color"
	case BlendLuminosity:
		return "Luminosity"
	}

	panic("Invalid blend mode specified")
}

// AddBlendMode creates a resource in the PDF document specifying the blend
// mode, as well as alpha values, to use in transparency.  Once created, the
// blend mode can be reused as often as necessary with calls to
// SetBlendModeWithIndex.
//
// Transparency requires a minimum version of 1.4.  Package users should ensure
// that the PDF document has the required minimum version listed with a call to
// SetMinimumVersion.
func (p *PDF) AddBlendMode(mode BlendMode, strokeAlpha, fillAlpha float32) int {
	ndx := len(p.gstates)
	p.gstates = append(p.gstates, gstateResource{
		BM: mode,
		ca: fillAlpha,
		CA: strokeAlpha,
	})
	return ndx
}

// SetBlendMode is a convenience function for calling AddBlendMode followed by
// SetBlendModeWithIndex.
func (p *PDF) SetBlendMode(mode BlendMode, strokeAlpha, fillAlpha float32) int {
	ndx := p.AddBlendMode(mode, strokeAlpha, fillAlpha)
	p.SetBlendModeWithIndex(ndx)
	return ndx
}

// SetBlendModeWithIndex adds the information for the selected blend mode to the
// current graphics state.
//
// This function will panic if no page is currently open.  See the method CurrentPage.
func (p *PDF) SetBlendModeWithIndex(modeIndex int) {
	fmt.Fprintf(p.currentPage, "/G%d gs\n", modeIndex+1)
}
