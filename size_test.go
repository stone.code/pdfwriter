package pdf

import (
	"fmt"
	"testing"
)

func TestSize_Portrait(t *testing.T) {
	cases := []Size{A1, A2, A3, A4, A5, A6, Letter, Legal, Tabloid}

	for i, v := range cases {
		tmp := v.Portrait()
		if tmp != v {
			t.Errorf("Incorrect result for portrait on example %d, got %v", i, tmp)
		}
		if tmp.Orientation() != Portrait {
			t.Errorf("incorrect orientation reported for example %d", i)
		}
	}
}

func TestSize_Landscape(t *testing.T) {
	cases := []Size{A1, A2, A3, A4, A5, A6, Letter, Legal, Tabloid}

	for i, v := range cases {
		tmp := v.Landscape()
		if tmp.Width != v.Height || tmp.Height != v.Width {
			t.Errorf("Incorrect result for landscape on example %d, got %v", i, tmp)
		}
		if tmp.Orientation() != Landscape {
			t.Errorf("Incorrect orientation reported for example %d", i)
		}
		if tmp.Landscape() != tmp {
			t.Errorf("Incorrect result for landscape on example %d, got %v", i, tmp)
		}

		if tmp.Portrait() != v {
			t.Errorf("Incorrect result for portrait on example %d,", i)
		}
	}
}

func TestSize_String(t *testing.T) {
	cases := []struct {
		size Size
		name string
	}{
		{A1, "A1"}, {A2, "A2"}, {A3, "A3"}, {A4, "A4"}, {A5, "A5"}, {A6, "A6"},
		{Letter, "Letter"}, {Legal, "Legal"}, {Tabloid, "Tabloid"},
		{Size{1 * Inch, 1 * Inch}, "Custom"}, {Size{3 * Inch, 4 * Inch}, "Custom"},
	}

	for i, v := range cases {
		if out := v.size.String(); out != v.name {
			t.Errorf("Incorrect string for example %d (%s), got %s", i, v.name, out)
		}
		if out := v.size.Portrait().String(); out != v.name {
			t.Errorf("Incorrect string for example %d (%s), got %s", i, v.name, out)
		}
		if out := v.size.Landscape().String(); out != v.name {
			t.Errorf("Incorrect string for example %d (%s), got %s", i, v.name, out)
		}
	}
}

func ExampleSize_String() {
	// We can check the name of the known page sizes.
	fmt.Printf("Dimensions of '%s' are %.0f x %.0f points.\n",
		Letter, Letter.Width, Letter.Height)

	// Output:
	// Dimensions of 'Letter' are 612 x 792 points.
}
