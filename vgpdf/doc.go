// Package vgpdf implements the vg.Canvas interface using pdfwriter.
// Unlike the PDF driver supplied by gonum.org/v1/plot/vg, which only supports
// creating a PDF with a single plot, this driver supports creating multiple
// plots in the PDF.  These plots can either be on individual pages, or
// interspersed with text and other elements.
package vgpdf
