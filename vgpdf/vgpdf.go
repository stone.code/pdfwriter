package vgpdf

import (
	"gitlab.com/stone.code/pdfwriter"
	"gonum.org/v1/plot/vg"
	"image"
	"image/color"
	"math"
)

// Canvas represents a page, or a section of a page, used for drawing 2D
// graphics, and which supports the interface gonum.org/v1/plot/vg.Canvas.
// Using this type, users can create plots using gonum.org/v1/plot, and output
// those plots into a PDF document.
type Canvas struct {
	doc       *pdf.PDF
	size      pdf.Size
	lineWidth pdf.PT
}

// New creates a new canvas where drawing operations will occur in a specified
// region of the current page.  The graphics state will be modified, so
// users will need to manage their graphics state using Push, Pop, or WithState,
// if they want to resume drawing after plotting.
func New(doc *pdf.PDF, pos pdf.Point, size pdf.Size) *Canvas {
	c := &Canvas{
		doc:  doc,
		size: size,
	}
	doc.Translate(pos.X, pos.Y)
	doc.SetLineWidth(1) // See documentation gonum.org/v1/plot/vg#Canvas
	doc.SetDrawGray(0)  // See documentation gonum.org/v1/plot/vg#Canvas
	doc.SetFillGray(0)  // See documentation gonum.org/v1/plot/vg#Canvas
	vg.Initialize(c)
	return c
}

// NewPage creates a new canvas where drawing operations will occur on a new
// page in the PDF document.
func NewPage(doc *pdf.PDF, size pdf.Size) *Canvas {
	doc.AddPageWithSize(size)

	c := &Canvas{
		doc:  doc,
		size: size,
	}
	doc.SetLineWidth(1) // See documentation gonum.org/v1/plot/vg#Canvas
	doc.SetDrawGray(0)  // See documentation gonum.org/v1/plot/vg#Canvas
	doc.SetFillGray(0)  // See documentation gonum.org/v1/plot/vg#Canvas
	vg.Initialize(c)
	return c
}

// Size returns the size of the canvas.
// This method supports the interface gonum.org/v1/plot/vg.CanvaSizer.
func (c *Canvas) Size() (w, h vg.Length) {
	// Although different types, both packages use postscript points as the
	// unit of measure
	return vg.Length(c.size.Width), vg.Length(c.size.Height)
}

// SetLineWidth supports the interface gonum.org/v1/plot/vg.Canvas.
func (c *Canvas) SetLineWidth(w vg.Length) {
	if w > 0 {
		c.doc.SetLineWidth(pdf.PT(w))
	}
	c.lineWidth = pdf.PT(w)
}

// SetLineDash supports the interface gonum.org/v1/plot/vg.Canvas.
func (c *Canvas) SetLineDash(dashes []vg.Length, offs vg.Length) {
	// Although compatible, we cannot cast without copying.
	dashes2 := make([]pdf.PT, 0, len(dashes))
	for _, v := range dashes {
		dashes2 = append(dashes2, pdf.PT(v))
	}
	c.doc.SetDashPattern(dashes2, pdf.PT(offs))
}

// SetColor supports the interface gonum.org/v1/plot/vg.Canvas.
func (c *Canvas) SetColor(clr color.Color) {
	if clr == nil {
		// According to documentation, need to support nil.
		c.doc.SetDrawGray(0)
		c.doc.SetFillGray(0)
		return
	}
	c.doc.SetDrawColor(clr)
	c.doc.SetFillColor(clr)
}

// Rotate supports the interface gonum.org/v1/plot/vg.Canvas.
func (c *Canvas) Rotate(r float64) {
	c.doc.Rotate(r)
}

// Translate supports the interface gonum.org/v1/plot/vg.Canvas.
func (c *Canvas) Translate(pt vg.Point) {
	c.doc.Translate(pdf.PT(pt.X), pdf.PT(pt.Y))
}

// Scale supports the interface gonum.org/v1/plot/vg.Canvas.
func (c *Canvas) Scale(x float64, y float64) {
	c.doc.Transform(float32(x), 0, 0, float32(y), 0, 0)
}

// Push supports the interface gonum.org/v1/plot/vg.Canvas.
func (c *Canvas) Push() {
	c.doc.PushState()
}

// Pop supports the interface gonum.org/v1/plot/vg.Canvas.
func (c *Canvas) Pop() {
	c.doc.PopState()
}

func createPath(doc *pdf.PDF, p vg.Path) *pdf.Path {
	path := doc.Path()
	for _, comp := range p {
		switch comp.Type {
		case vg.MoveComp:
			path.MoveTo(pdf.PT(comp.Pos.X), pdf.PT(comp.Pos.Y))
		case vg.LineComp:
			path.LineTo(pdf.PT(comp.Pos.X), pdf.PT(comp.Pos.Y))
		case vg.ArcComp:
			arc(path, comp)
		case vg.CloseComp:
			path.Close(0)
		default:
			panic("unknown path component type")
		}
	}

	return path
}

// Stroke supports the interface gonum.org/v1/plot/vg.Canvas.
func (c *Canvas) Stroke(p vg.Path) {
	if c.lineWidth > 0 {
		path := createPath(c.doc, p)
		path.Draw(pdf.Stroke)
	}
}

// Fill supports the interface gonum.org/v1/plot/vg.Canvas.
func (c *Canvas) Fill(p vg.Path) {
	path := createPath(c.doc, p)
	path.Draw(pdf.Fill)
}

// FillString supports the interface gonum.org/v1/plot/vg.Canvas.
func (c *Canvas) FillString(fnt vg.Font, pt vg.Point, str string) {
	switch fnt.Name() {
	case "Courier":
		c.doc.SetFont(pdf.Courier, pdf.Normal, pdf.PT(fnt.Size))
	case "Courier-Bold":
		c.doc.SetFont(pdf.Courier, pdf.Bold, pdf.PT(fnt.Size))
	case "Courier-Oblique":
		c.doc.SetFont(pdf.Courier, pdf.Italic, pdf.PT(fnt.Size))
	case "Courier-BoldOblique":
		c.doc.SetFont(pdf.Courier, pdf.Bold|pdf.Italic, pdf.PT(fnt.Size))

	case "Helvetica":
		c.doc.SetFont(pdf.Helvetica, pdf.Normal, pdf.PT(fnt.Size))
	case "Helvetica-Bold":
		c.doc.SetFont(pdf.Helvetica, pdf.Bold, pdf.PT(fnt.Size))
	case "Helvetica-Oblique":
		c.doc.SetFont(pdf.Helvetica, pdf.Italic, pdf.PT(fnt.Size))
	case "Helvetica-BoldOblique":
		c.doc.SetFont(pdf.Helvetica, pdf.Bold|pdf.Italic, pdf.PT(fnt.Size))

	case "Times-Roman":
		c.doc.SetFont(pdf.Times, pdf.Normal, pdf.PT(fnt.Size))
	case "Times-Bold":
		c.doc.SetFont(pdf.Times, pdf.Bold, pdf.PT(fnt.Size))
	case "Times-Italic":
		c.doc.SetFont(pdf.Times, pdf.Italic, pdf.PT(fnt.Size))
	case "Times-BoldItalic":
		c.doc.SetFont(pdf.Times, pdf.Bold|pdf.Italic, pdf.PT(fnt.Size))

	default:
		panic("unrecognized font name " + fnt.Name())
	}
	c.doc.Text(str, pdf.PT(pt.X), pdf.PT(pt.Y))
}

// DrawImage supports the interface gonum.org/v1/plot/vg.Canvas.
func (c *Canvas) DrawImage(rect vg.Rectangle, img image.Image) {
	c.doc.Image(img, pdf.Rectangle{
		pdf.Point{pdf.PT(rect.Min.X), pdf.PT(rect.Min.Y)},
		pdf.Point{pdf.PT(rect.Max.X), pdf.PT(rect.Max.Y)},
	})
}

// Approximate a circular arc using multiple
// cubic Bézier curves, one for each π/2 segment.
//
// This is from:
// 	http://hansmuller-flex.blogspot.com/2011/04/approximating-circular-arc-with-cubic.html
func arc(p *pdf.Path, comp vg.PathComp) {
	x0 := comp.Pos.X + comp.Radius*vg.Length(math.Cos(comp.Start))
	y0 := comp.Pos.Y + comp.Radius*vg.Length(math.Sin(comp.Start))
	p.LineTo(pdf.PT(x0), pdf.PT(y0))

	a1 := comp.Start
	end := a1 + comp.Angle
	sign := 1.0
	if end < a1 {
		sign = -1.0
	}
	left := math.Abs(comp.Angle)

	// Square root of the machine epsilon for IEEE 64-bit floating
	// point values.  This is the equality threshold recommended
	// in Numerical Recipes, if I recall correctly—it's small enough.
	const epsilon = 1.4901161193847656e-08

	for left > epsilon {
		a2 := a1 + sign*math.Min(math.Pi/2, left)
		partialArc(p, comp.Pos.X, comp.Pos.Y, comp.Radius, a1, a2)
		left -= math.Abs(a2 - a1)
		a1 = a2
	}
}

// Approximate a circular arc of fewer than π/2
// radians with cubic Bézier curve.
func partialArc(p *pdf.Path, x, y, r vg.Length, a1, a2 float64) {
	a := (a2 - a1) / 2
	x4 := r * vg.Length(math.Cos(a))
	y4 := r * vg.Length(math.Sin(a))
	x1 := x4
	y1 := -y4

	const k = 0.5522847498 // some magic constant
	f := k * vg.Length(math.Tan(a))
	x2 := x1 + f*y4
	y2 := y1 + f*x4
	x3 := x2
	y3 := -y2

	// Rotate and translate points into position.
	ar := a + a1
	sinar := vg.Length(math.Sin(ar))
	cosar := vg.Length(math.Cos(ar))
	x2r := x2*cosar - y2*sinar + x
	y2r := x2*sinar + y2*cosar + y
	x3r := x3*cosar - y3*sinar + x
	y3r := x3*sinar + y3*cosar + y
	x4 = r*vg.Length(math.Cos(a2)) + x
	y4 = r*vg.Length(math.Sin(a2)) + y
	p.CurveTo(pdf.PT(x2r), pdf.PT(y2r), pdf.PT(x3r), pdf.PT(y3r), pdf.PT(x4), pdf.PT(y4))
}
