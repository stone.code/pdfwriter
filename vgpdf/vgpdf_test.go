package vgpdf_test

import (
	"os"
	"path/filepath"
	"testing"

	pdf "gitlab.com/stone.code/pdfwriter"
	"gitlab.com/stone.code/pdfwriter/vgpdf"
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
	"gonum.org/v1/plot/vg/draw"
)

var (
	_ vg.Canvas      = &vgpdf.Canvas{}
	_ vg.CanvasSizer = &vgpdf.Canvas{}
)

func writeFile(t *testing.T, doc *pdf.PDF) {
	data, err := doc.Close()
	if err != nil {
		t.Fatalf("Error, %s", err)
	}

	filename := filepath.Join(t.Name() + ".pdf")
	file, err := os.OpenFile(filename, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0666)
	if err != nil {
		t.Fatalf("Could not open file, %s", err)
	}
	defer file.Close()

	_, err = data.WriteTo(file)
	if err != nil {
		t.Fatalf("Could not write file, %s", err)
	}

	offset, err := file.Seek(0, 2)
	if err != nil {
		t.Fatalf("Could not seek file, %s", err)
	}
	t.Logf("File size for %s is %d\n", filename, offset)
}

func ExampleNewPage() {
	// Create a new PDF document.
	doc := pdf.New(pdf.Letter)
	// Create a new page in the PDF document, and use it as a canvas for plotting.
	canvas := vgpdf.NewPage(doc, pdf.Size{4 * 72, 3 * 72})

	// Use the canvas ...
	_ = canvas
}

func TestNew(t *testing.T) {
	doc := pdf.New(pdf.Letter)
	doc.AddPage()
	canvas := vgpdf.New(doc, pdf.Point{1 * 72, 1 * 72}, pdf.Size{4 * 72, 3 * 72})

	drawFigure(t, canvas)
	writeFile(t, doc)
}

func TestNewPage(t *testing.T) {
	doc := pdf.New(pdf.Letter)
	canvas := vgpdf.NewPage(doc, pdf.Size{4 * 72, 3 * 72})

	drawFigure(t, canvas)
	writeFile(t, doc)
}

func drawFigure(t *testing.T, canvas vg.CanvasSizer) {
	p, err := plot.New()
	if err != nil {
		t.Fatalf("Error, %s", err)
	}

	plotter.DefaultLineStyle.Width = vg.Points(1)
	plotter.DefaultGlyphStyle.Radius = vg.Points(3)

	p.Y.Tick.Marker = plot.ConstantTicks([]plot.Tick{
		{0, "0"}, {0.25, ""}, {0.5, "0.5"}, {0.75, ""}, {1, "1"},
	})
	p.X.Tick.Marker = plot.ConstantTicks([]plot.Tick{
		{0, "0"}, {0.25, ""}, {0.5, "0.5"}, {0.75, ""}, {1, "1"},
	})

	pts := plotter.XYs{{0, 0}, {0, 1}, {0.5, 1}, {0.5, 0.6}, {0, 0.6}}
	line, err := plotter.NewLine(pts)
	if err != nil {
		t.Fatalf("Error, %s", err)
	}
	scatter, err := plotter.NewScatter(pts)
	if err != nil {
		t.Fatalf("Error, %s", err)
	}
	p.Add(line, scatter)

	pts = plotter.XYs{{1, 0}, {0.75, 0}, {0.75, 0.75}}
	line, err = plotter.NewLine(pts)
	if err != nil {
		t.Fatalf("Error, %s", err)
	}
	scatter, err = plotter.NewScatter(pts)
	if err != nil {
		t.Fatalf("Error, %s", err)
	}
	p.Add(line, scatter)

	pts = plotter.XYs{{0.5, 0.5}, {1, 0.5}}
	line, err = plotter.NewLine(pts)
	if err != nil {
		t.Fatalf("Error, %s", err)
	}
	scatter, err = plotter.NewScatter(pts)
	if err != nil {
		t.Fatalf("Error, %s", err)
	}
	p.Add(line, scatter)

	p.Draw(draw.New(canvas))
}
