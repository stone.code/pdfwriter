package pdf

import (
	"testing"
)

func TestEncrypt(t *testing.T) {
	pdf := New(Size{1 * Inch, 1 * Inch})
	pdf.SetTitle("Test Text")
	pdf.SetKeywords("pdfwriter, ça va")
	pdf.SetAuthor("Hello, 世界")
	pdf.SetEncryption("owner", "user", PrintPermission|Revision3Permissions)
	pdf.AddPage()
	pdf.Line(Point{0, 0}, Point{72, 72})
	mustSetFont(t, pdf, Times, Normal, 10)
	pdf.Text("Can't read", 8, 36)
	pdf.Text("this.", 8, 24)

	// Close and write the PDF file.
	writeFile(t, pdf)
}
