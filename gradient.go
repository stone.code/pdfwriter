package pdf

import (
	"bytes"
	"fmt"
	"image/color"
)

type gradientResource interface {
	write(p *PDF, w *bytes.Buffer)
}

type linearRGBGradient struct {
	a, b   Point
	color1 color.RGBA
	color2 color.RGBA
}

// AddLinearGradientRGB adds a linear gradient definition to the PDF document.
func (p *PDF) AddLinearGradientRGB(a, b Point, r1, g1, b1, r2, g2, b2 uint8) int {
	p.gradients = append(p.gradients, &linearRGBGradient{
		a: a, b: b,
		color1: color.RGBA{r1, g1, b1, 255},
		color2: color.RGBA{r2, g2, b2, 255},
	})
	return len(p.gradients)
}

func (g *linearRGBGradient) write(p *PDF, w *bytes.Buffer) {
	// Write the shading object
	ndx := p.newobj(w)
	fmt.Fprintf(w, "<</ShadingType 2 /ColorSpace /DeviceRGB /Coords [%.4f %.4f %.4f %.4f] /Function %d 0 R /Extend [true true]>>\n", g.a.X, g.a.Y, g.b.X, g.b.Y, ndx+1)
	p.endobj(w)

	// Write the function
	p.newobj(w)
	fmt.Fprintf(w, "<</FunctionType 2 /Domain [0.0 1.0] /C0 [%.3f %.3f %.3f] /C1 [%.3f %.3f %.3f] /N 1>>\n",
		float32(g.color1.R)/0xFF, float32(g.color1.G)/0xFF, float32(g.color1.B)/0xFF,
		float32(g.color2.R)/0xFF, float32(g.color2.G)/0xFF, float32(g.color2.B)/0xFF,
	)
	p.endobj(w)
}

type radialRGBGradient struct {
	a, b   Point
	ra, rb PT
	color1 color.RGBA
	color2 color.RGBA
}

// AddRadialGradientRGB adds a radial gradient definition to the PDF document.
func (p *PDF) AddRadialGradientRGB(a Point, ra PT, b Point, rb PT, r1, g1, b1, r2, g2, b2 uint8) int {
	p.gradients = append(p.gradients, &radialRGBGradient{
		a: a, ra: ra,
		b: b, rb: rb,
		color1: color.RGBA{r1, g1, b1, 255},
		color2: color.RGBA{r2, g2, b2, 255},
	})
	return len(p.gradients)
}

func (g *radialRGBGradient) write(p *PDF, w *bytes.Buffer) {
	// Write the shading object
	ndx := p.newobj(w)
	fmt.Fprintf(w, "<</ShadingType 3 /ColorSpace /DeviceRGB /Coords [%.4f %.4f %.4f %.4f %.4f %.4f] /Function %d 0 R /Extend [true true]>>\n", g.a.X, g.a.Y, g.ra, g.b.X, g.b.Y, g.rb, ndx+1)
	p.endobj(w)

	// Write the function
	p.newobj(w)
	fmt.Fprintf(w, "<</FunctionType 2 /Domain [0.0 1.0] /C0 [%.3f %.3f %.3f] /C1 [%.3f %.3f %.3f] /N 1>>\n",
		float32(g.color1.R)/0xFF, float32(g.color1.G)/0xFF, float32(g.color1.B)/0xFF,
		float32(g.color2.R)/0xFF, float32(g.color2.G)/0xFF, float32(g.color2.B)/0xFF,
	)
	p.endobj(w)
}
