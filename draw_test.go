package pdf

import (
	"image/color"
	"os"
	"testing"
)

func drawStar(path *Path, offset PT) *Path {
	path.MoveTo(36+offset, 18)
	path.LineTo(18+offset, 54)
	path.LineTo(54+offset, 36)
	path.LineTo(18+offset, 36)
	path.LineTo(54+offset, 54)
	return path
}

func TestColor(t *testing.T) {
	pdf := New(Size{5 * Inch, 2.5 * Inch})
	pdf.DisableCompression()

	// RGB
	pdf.AddPage()
	mustSetFont(t, pdf, Helvetica, Bold, 12)
	pdf.Text("RGB", 1*Inch, 2*Inch)
	pdf.SetFillRGB(0xff, 0, 0)
	pdf.SetDrawRGB(0, 0xff, 0)
	pdf.Rectangle(1*Inch, 0.5*Inch, 1*Inch, 1*Inch, Stroke|Fill)
	pdf.SetFillRGB(0, 0xff, 0)
	pdf.SetDrawRGB(0, 0, 0xff)
	pdf.Rectangle(2*Inch, 0.5*Inch, 1*Inch, 1*Inch, Stroke|Fill)
	pdf.SetFillRGB(0, 0, 0xff)
	pdf.SetDrawRGB(0xff, 0, 0)
	pdf.Rectangle(3*Inch, 0.5*Inch, 1*Inch, 1*Inch, Stroke|Fill)

	// RGB (Muted)
	pdf.AddPage()
	mustSetFont(t, pdf, Helvetica, Bold, 12)
	pdf.Text("RGB (Muted)", 1*Inch, 2*Inch)
	pdf.SetFillRGB(0xc0, 0, 0)
	pdf.SetDrawRGB(0, 0xc0, 0)
	pdf.Rectangle(1*Inch, 0.5*Inch, 1*Inch, 1*Inch, Stroke|Fill)
	pdf.SetFillRGB(0, 0xc0, 0)
	pdf.SetDrawRGB(0, 0, 0xc0)
	pdf.Rectangle(2*Inch, 0.5*Inch, 1*Inch, 1*Inch, Stroke|Fill)
	pdf.SetFillRGB(0, 0, 0xc0)
	pdf.SetDrawRGB(0xc0, 0, 0)
	pdf.Rectangle(3*Inch, 0.5*Inch, 1*Inch, 1*Inch, Stroke|Fill)

	// Gray
	pdf.AddPage()
	pdf.SetLineWidth(2 * Pt)
	mustSetFont(t, pdf, Helvetica, Bold, 12)
	pdf.Text("Gray", 1*Inch, 2*Inch)
	pdf.SetFillGray(0x40)
	pdf.SetDrawGray(0x80)
	pdf.Rectangle(1*Inch, 0.5*Inch, 1*Inch, 1*Inch, Stroke|Fill)
	pdf.SetFillGray(0x80)
	pdf.SetDrawGray(0xc0)
	pdf.Rectangle(2*Inch, 0.5*Inch, 1*Inch, 1*Inch, Stroke|Fill)
	pdf.SetFillGray(0xc0)
	pdf.SetDrawGray(0x40)
	pdf.Rectangle(3*Inch, 0.5*Inch, 1*Inch, 1*Inch, Stroke|Fill)

	// CMYK
	pdf.AddPage()
	pdf.SetLineWidth(2 * Pt)
	mustSetFont(t, pdf, Helvetica, Bold, 12)
	pdf.Text("CMYK", 1*Inch, 2*Inch)
	pdf.SetFillCMYK(0xff, 0, 0, 0)
	pdf.SetDrawCMYK(0, 0xff, 0, 0)
	pdf.Rectangle(1*Inch, 0.5*Inch, 1*Inch, 1*Inch, Stroke|Fill)
	pdf.SetFillCMYK(0, 0xff, 0, 0)
	pdf.SetDrawCMYK(0, 0, 0xff, 0)
	pdf.Rectangle(2*Inch, 0.5*Inch, 1*Inch, 1*Inch, Stroke|Fill)
	pdf.SetFillCMYK(0, 0, 0xff, 0)
	pdf.SetDrawCMYK(0xff, 0, 0, 0)
	pdf.Rectangle(3*Inch, 0.5*Inch, 1*Inch, 1*Inch, Stroke|Fill)

	// CMYK (Muted)
	pdf.AddPage()
	pdf.SetLineWidth(2 * Pt)
	mustSetFont(t, pdf, Helvetica, Bold, 12)
	pdf.Text("CMYK (Muted)", 1*Inch, 2*Inch)
	pdf.SetFillCMYK(0xff, 0, 0, 0x80)
	pdf.SetDrawCMYK(0, 0xff, 0, 0x80)
	pdf.Rectangle(1*Inch, 0.5*Inch, 1*Inch, 1*Inch, Stroke|Fill)
	pdf.SetFillCMYK(0, 0xff, 0, 0x80)
	pdf.SetDrawCMYK(0, 0, 0xff, 0x80)
	pdf.Rectangle(2*Inch, 0.5*Inch, 1*Inch, 1*Inch, Stroke|Fill)
	pdf.SetFillCMYK(0, 0, 0xff, 0x80)
	pdf.SetDrawCMYK(0xff, 0, 0, 0x80)
	pdf.Rectangle(3*Inch, 0.5*Inch, 1*Inch, 1*Inch, Stroke|Fill)

	// Close and write the PDF file.
	writeFile(t, pdf)
}

func TestDraw(t *testing.T) {
	pdf := New(Size{5 * Inch, 1.5 * Inch})
	pdf.DisableCompression()

	title := func(s string) {
		mustSetFont(t, pdf, Helvetica, Normal, 12)
		pdf.Text(s, 12, 1.5*Inch-12)
	}

	// Draw operations
	pdf.AddPage()
	title("Drawing operations (Rectangle)")
	pdf.SetDrawRGB(0, 0, 255)
	pdf.SetFillRGB(0, 128, 0)
	pdf.Rectangle(18, 18, 36, 36, Stroke)
	pdf.Rectangle(18+1*Inch, 18, 36, 36, Fill)
	pdf.Rectangle(18+2*Inch, 18, 36, 36, Fill|Stroke)
	pdf.Rectangle(18+3*Inch, 18, 36, 36, Fill|EvenOdd)
	pdf.Rectangle(18+4*Inch, 18, 36, 36, Fill|Stroke|EvenOdd)

	// Draw operations
	pdf.AddPage()
	title("Drawing operations (Star)")
	pdf.SetDrawRGB(0, 0, 255)
	pdf.SetFillRGB(0, 128, 0)
	drawStar(pdf.Path(), 0).Close(Stroke)
	drawStar(pdf.Path(), 1*Inch).Close(Fill)
	drawStar(pdf.Path(), 2*Inch).Close(Fill | Stroke)
	drawStar(pdf.Path(), 3*Inch).Close(Fill | EvenOdd)
	drawStar(pdf.Path(), 4*Inch).Close(Fill | Stroke | EvenOdd)

	// Color spaces when filling
	pdf.AddPage()
	title("Color spaces when filling")
	pdf.SetFillGray(0x80)
	drawStar(pdf.Path(), 0).Close(Fill)
	pdf.SetFillGray16(0x8000)
	drawStar(pdf.Path(), 1*Inch).Close(Fill)
	pdf.SetFillCMYK(0, 0, 0xff, 0)
	drawStar(pdf.Path(), 2*Inch).Close(Fill)
	pdf.SetFillColor(color.RGBA64{0xffff, 0, 0, 0xffff})
	drawStar(pdf.Path(), 3*Inch).Close(Fill)
	pdf.SetFillColor(color.RGBA64{0, 0xffff, 0, 0xffff})
	drawStar(pdf.Path(), 4*Inch).Close(Fill)

	// Color spaces when drawing
	pdf.AddPage()
	title("Color spaces when drawing")
	pdf.SetDrawGray(0x80)
	drawStar(pdf.Path(), 0).Close(Stroke)
	pdf.SetDrawGray16(0x8000)
	drawStar(pdf.Path(), 1*Inch).Close(Stroke)
	//pdf.SetDrawCMYK(0, 0, 0xff, 0)
	drawStar(pdf.Path(), 2*Inch).Close(Stroke)
	pdf.SetDrawColor(color.RGBA64{0xffff, 0, 0, 0xffff})
	drawStar(pdf.Path(), 3*Inch).Close(Stroke)
	pdf.SetDrawColor(color.RGBA64{0, 0xffff, 0, 0xffff})
	drawStar(pdf.Path(), 4*Inch).Close(Stroke)

	// Line joins
	pdf.AddPage()
	title("Line Joins / Line Width / Dashes")
	pdf.SetJoin(MiterJoin)
	drawStar(pdf.Path(), 0).Close(Stroke)
	pdf.SetJoin(RoundJoin)
	drawStar(pdf.Path(), 1*Inch).Close(Stroke)
	pdf.SetJoin(BevelJoin)
	drawStar(pdf.Path(), 2*Inch).Close(Stroke)
	pdf.SetJoin(MiterJoin)
	pdf.SetLineWidth(2 * Pt)
	pdf.SetMiterLimit(4 * Pt)
	drawStar(pdf.Path(), 3*Inch).Close(Stroke)
	pdf.SetJoin(RoundJoin)
	pdf.SetLineWidth(2 * Pt)
	pdf.SetDashPattern([]PT{3, 2}, 1.5)
	drawStar(pdf.Path(), 4*Inch).Close(Stroke)

	// Line caps
	pdf.AddPageWithSize(Size{72, 72})
	title("Line caps")
	pdf.SetLineWidth(10)
	pdf.SetCap(ButtCap)
	pdf.Line(Point{18, 18}, Point{72 - 18, 18})
	pdf.SetCap(RoundCap)
	pdf.Line(Point{18, 30}, Point{72 - 18, 30})
	pdf.SetCap(SquareCap)
	pdf.Line(Point{18, 42}, Point{72 - 18, 42})
	path := pdf.Path()
	path.MoveTo(18, 54)
	path.LineTo(27, 54)
	path.LineTo(36, 54+4.5)
	path.LineTo(72-27, 54)
	path.LineTo(72-18, 54)
	path.Draw(Stroke)

	// Cliping
	pdf.AddPage()
	title("Clipping")
	ps := pdf.CurrentPageSize()
	path = pdf.Rectangle(6, 6, ps.Width-12, ps.Height-12, 0)
	path.Clip()
	pdf.Line(Point{}, Point{ps.Width, ps.Height})
	pdf.Line(Point{ps.Width, 0}, Point{0, ps.Height})

	pdf.AddPage()
	title("Gradients")
	gradientNdx := pdf.AddLinearGradientRGB(Point{12, 60}, Point{60, 12}, 0, 255, 0, 0, 0, 255)
	path = pdf.Rectangle(12, 12, 48, 48, Nop)
	path.Shade(gradientNdx)
	_ = pdf.Rectangle(12, 12, 48, 48, Stroke)
	pdf.PushState()
	path = pdf.Rectangle(12+72, 12, 48, 48, 0)
	path.Clip()
	pdf.Shade(gradientNdx)
	pdf.PopState()
	pdf.Rectangle(12+72, 12, 48, 48, Stroke)
	gradientNdx2 := pdf.AddRadialGradientRGB(Point{12 + 144 + 24, 36}, 0, Point{12 + 144 + 24, 36}, 24, 0, 255, 0, 0, 0, 255)
	pdf.Rectangle(12+144, 12, 48, 48, Nop).Shade(gradientNdx2)
	pdf.Rectangle(12+144, 12, 48, 48, Stroke)

	// Transforms
	pdf.AddPage()
	title("Transforms")
	pdf.WithState(func() {
		pdf.Translate(36, 36)
		pdf.Rectangle(-18, -18, 36, 36, Stroke)
	})
	pdf.WithState(func() {
		pdf.Translate(36+72, 36)
		pdf.Rotate(3.14 / 4)
		pdf.Rectangle(-18, -18, 36, 36, Stroke)
	})
	pdf.WithState(func() {
		pdf.Translate(36+2*72, 36)
		pdf.Scale(1.5)
		pdf.Rectangle(-18, -18, 36, 36, Stroke)
	})

	// Close and write the PDF file.
	writeFile(t, pdf)
}

func TestPath(t *testing.T) {
	pdf := New(Size{1 * Inch, 1 * Inch})
	pdf.AddPage()
	pdf.SetDrawRGB(0, 255, 0)
	pdf.SetFillRGB(255, 0, 0)
	pdf.SetJoin(RoundJoin)
	path := pdf.Path()
	path.MoveTo(18, 18)
	path.CurveTo(32, 27, 40, 44, 54, 54)
	path.LineTo(18, 54)
	path.CurveTo(8, 48, 8, 48, 18, 36)
	path.Close(Fill | Stroke)

	// Close and write the PDF file.
	writeFile(t, pdf)
}

func TestJPEG(t *testing.T) {
	file, err := os.Open("./test1.jpg")
	if err != nil {
		t.Fatalf("Could not open file, %s", err)
	}
	defer file.Close()

	pdf := New(Size{1 * Inch, 1 * Inch})
	imageNdx, err := pdf.AddJPEG(file)
	if err != nil {
		t.Fatalf("Could not add image, %s", err)
	}
	pdf.AddPage()
	pdf.ImageByIndex(imageNdx, Rect(18, 18, 72-18, 72-18))

	// Close and write the PDF file.
	writeFile(t, pdf)
}

func ExamplePDF_WithState() {
	pageSize := Size{1 * Inch, 1 * Inch}
	doc := New(pageSize)
	doc.SetLineWidth(1 * Pt)
	doc.Line(Point{0, 24}, Point{72, 24})
	doc.WithState(func() {
		doc.SetLineWidth(2 * Pt)
		doc.Line(Point{0, 36}, Point{72, 36})
	})
	// Note, this drawing command will use a linewidth of 1pt.  The change to the
	// linewidth for the prior command was reversed when the callback was finished.
	doc.Line(Point{0, 48}, Point{72, 48})
}
