package pdf

import (
	"golang.org/x/image/font/gofont/gobold"
	"golang.org/x/image/font/gofont/gomono"
	"golang.org/x/image/font/gofont/goregular"
	"testing"
)

func mustSetFont(t *testing.T, doc *PDF, family FontFamily, style FontStyle, size PT) int {
	ndx, err := doc.SetFont(family, style, size)
	if err != nil {
		t.Fatalf("could not set font: %s", err)
	}
	return ndx
}

func TestAddFont(t *testing.T) {
	pdf := New(Letter)
	pdf.DisableCompression()

	pdf.AddPage()
	_, err := pdf.AddBuiltinFont("dne", Bold)
	if err == nil {
		t.Errorf("Failed to return error for non-existent font")
	}

	// Add the font
	ndx, err := pdf.AddBuiltinFont(Times, Normal)
	if err != nil {
		t.Errorf("Unexpected error when adding builting font: %s", err)
	}
	if ndx != 2 {
		// Font indices are returned base 1, not base 0.
		// Font indices are times 2 - low bit is for underline
		t.Errorf("got %d, want 0", ndx)
	}

	// Add again, need to get the same index
	ndx, err = pdf.AddBuiltinFont(Times, Normal)
	if err != nil {
		t.Errorf("Unexpected error when adding builting font: %s", err)
	}
	if ndx != 2 {
		// Font indices are returned base 1, not base 0.
		// Font indices are times 2 - low bit is for underline
		t.Errorf("got %d, want 0", ndx)
	}
}

func TestTTF(t *testing.T) {
	pdf := New(Letter.Landscape())
	pdf.DisableCompression()

	pdf.AddPage()
	// Check that we can write with a builtin font
	if len(pdf.fonts) != 0 {
		t.Errorf("PDF document already has fonts registered!")
	}
	ndx, err := pdf.SetFont(Times, Normal, 12)
	if ndx != 2 {
		t.Errorf("Unexpected font index returned")
	}
	if err != nil {
		t.Errorf("Error on set font, %s", err)
	}
	pdf.SetFillGray(192)
	pdf.Rectangle(72, 72, 9*72, 6.5*72, Fill)
	pdf.SetFillGray(0)
	for i, v := range []FontFamily{Courier, Times, Helvetica} {
		_, err := pdf.SetFont(v, Normal, 12)
		if err != nil {
			t.Errorf("could not set font: %s", err)
		}
		pdf.Text("("+string(v)+")    Hello, this is a test", 72, 7.5*72-12-PT(i*16))
	}

	// Check that we can write with a TTF
	for i, v := range [][]byte{goregular.TTF, gobold.TTF, gomono.TTF} {
		ndx, name, err := pdf.AddFontTTF(v)
		if err != nil {
			t.Fatalf("Could not add TTF font, %s", err)
		}
		t.Logf("Font name: %s", name)
		ndx2, err := pdf.SetFont(name, Normal, 12)
		if err != nil {
			t.Errorf("could not set font: %s", err)
		}
		if ndx != ndx2 {
			t.Errorf("Mismatch on font index for truetype font")
		}
		pdf.Text(helloASCII, 72, 7.5*72-PT(5+4*i)*16)
		pdf.Text(helloLATIN8, 72, 7.5*72-PT(5+4*i+1)*16)
		pdf.Text(helloUTF8, 72, 7.5*72-PT(5+4*i+2)*16)
	}

	// Use of text split and justification
	pdf.AddPageWithSize(Size{72, 72})
	ndx, err = pdf.SetFont("GoRegular", Normal, 8)
	if err != nil {
		t.Errorf("Unexpected error when setting font, %s", err)
	}
	pdf.SetDrawGray(128)
	pdf.Line(Point{8, 0}, Point{8, 72})
	pdf.Line(Point{72 - 8, 0}, Point{72 - 8, 72})
	lines := pdf.TextSplit("This is a test of text reflow.  How well will it work?", ndx, 8, 72-16)
	for i, v := range lines {
		if i < len(lines)-1 {
			w, _ := pdf.TextWidth(v)
			pdf.SetCharacterSpacing(float32(72-16-w) / float32(len(v)-1))
		} else {
			pdf.SetCharacterSpacing(0)
		}
		pdf.Text(v, 8, 72-8-10*PT(i))
	}

	writeFile(t, pdf)
}
