package pdf

import (
	"crypto/md5"
	"crypto/rc4"
)

// Permissions is a bit field describing the user permissions for an encrypted
// PDF document.  See section 3.5.2 of the reference.
type Permissions uint16

// Available permission flags for encrypted PDF documents.
const (
	PrintPermission                Permissions = 1 << 2
	ModifyPermission               Permissions = 1 << 3
	CopyPermission                 Permissions = 1 << 4
	AnnotationsPermission          Permissions = 1 << 5
	FormPermission                 Permissions = 1 << 8
	ExtractTextAndImagesPermission Permissions = 1 << 9
	AssembleDocumentPermission     Permissions = 1 << 10
	FaithfulPrintPermission        Permissions = 1 << 11
	Revision3Permissions           Permissions = FormPermission | ExtractTextAndImagesPermission | AssembleDocumentPermission | FaithfulPrintPermission
)

var (
	padding = [...]byte{0x28, 0xBF, 0x4E, 0x5E, 0x4E, 0x75, 0x8A, 0x41,
		0x64, 0, 0x4E, 0x56, 0xFF, 0xFA, 0x01, 0x08,
		0x2E, 0x2E, 0, 0xB6, 0xD0, 0x68, 0x3E, 0x80,
		0x2F, 0x0C, 0xA9, 0xFE, 0x64, 0x53, 0x69, 0x7A}
)

func padPassword(out *[32]byte, password string) {
	if len(password) >= 32 {
		copy((*out)[:], password[:32])
		return
	}

	ndx := len(password)
	copy((*out)[:ndx], password)
	copy((*out)[ndx:], padding[:(32-ndx)])
}

func computeEncryptionKey(userPass string, ownerKey *[32]byte, permissions uint16, length int) [16]byte {
	// Step 1.  Pad or truncate the user password
	var userPPass [32]byte
	padPassword(&userPPass, userPass)

	// Step 2, 3, and 4
	var buf [32 + 32 + 4]byte
	copy(buf[:32], userPPass[:])
	copy(buf[32:64], (*ownerKey)[:])
	buf[64] = byte(permissions)
	buf[65] = byte(permissions >> 8)
	buf[66] = 0xFF
	buf[67] = 0xFF
	step4 := md5.Sum(buf[:])

	// Step 5
	// Note:  If file identifiers are implemented in the future, that data will
	// need to be added to the hash

	// Step 6:  Cycle MD5 hash 50 times
	if length > 0 {
		for i := 0; i < 25; i++ {
			tmp := md5.Sum(step4[:])
			step4 = md5.Sum(tmp[:])
		}
	}

	// Step 7:  Save the key
	return step4
}

func computeUValue(key []byte) [32]byte {
	// Step 1
	// Key already obtained

	// Step 2
	c, _ := rc4.NewCipher(key)
	var userKey [32]byte
	c.XORKeyStream(userKey[:], padding[:])

	// Step 3, store U value
	return userKey
}

func computeOValue(ownerPass string, userPass string, length int) [32]byte {
	// pad or truncate to 32 bytes
	var ownerPPass [32]byte
	padPassword(&ownerPPass, ownerPass)

	// Initialize MD5 hash
	step2 := md5.Sum(ownerPPass[:])

	// Cycle the MD5 hash 50 times
	if length > 0 {
		for i := 0; i < 25; i++ {
			tmp := md5.Sum(step2[:])
			step2 = md5.Sum(tmp[:])
		}
	}

	// Create an RC4 encryption key
	tmp := step2[:5]
	if length > 0 {
		tmp = step2[:length]
	}
	c, _ := rc4.NewCipher(tmp)

	// Pad the user password
	var userPPass [32]byte
	padPassword(&userPPass, userPass)

	// Encrypt the user password
	var ownerKey [32]byte
	c.XORKeyStream(ownerKey[:], userPPass[:])

	if length > 0 {
		panic("not implemented")
	}

	return ownerKey
}

// SetEncryption set the PDF document to be encrypted.  Users who can supply
// the owner password will get full access to the document, while users who
// can supply the user password will only get access to capabilities included
// in the permissions.
func (p *PDF) SetEncryption(ownerPassword, userPassword string, permissions Permissions) {
	if (permissions & 0x3) != 0 {
		panic("Invalid permissions set")
	}
	// Check if we need to go to version 3
	revision := uint(2)
	length := 0
	if permissions&Revision3Permissions != Revision3Permissions {
		revision = 3
		length = 5
		// TODO: expand length when possible
	}
	permissions = permissions | 0xF0C0

	p.encrypt.encryptionO = computeOValue(ownerPassword, userPassword, length)
	p.encrypt.encryptionKey = computeEncryptionKey(userPassword, &p.encrypt.encryptionO, uint16(permissions), length)
	if revision == 2 {
		p.encrypt.encryptionU = computeUValue(p.encrypt.encryptionKey[:5])
	} else {
		panic("not implemented")
	}
	p.encrypt.permissions = permissions
	p.encrypt.revision = revision
}

func (p *PDF) encryptData(data []byte, objectNdx int) {
	if p.encrypt.revision > 0 {
		// See algorithm 3.1 in the PDF reference

		// Step 2.  Extend the encryption key with the low-order three bytes of the
		// object index and the low-order two bytes of the generation number.
		ndx := uint32(objectNdx)
		b := append([]byte(nil), p.encrypt.encryptionKey[:5]...)
		b = append(b, byte(ndx), byte(ndx>>8), byte(ndx>>16), 0, 0)

		// Step 3.  Initialize the MD5sum
		s := md5.Sum(b)

		// Step 4.  Initialize the RC4 cypher
		c, _ := rc4.NewCipher(s[:10])
		// Encode the data
		c.XORKeyStream(data, data)
	}
}
