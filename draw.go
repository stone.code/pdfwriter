package pdf

import (
	"bytes"
	"fmt"
	"image"
	"image/color"
	"math"
)

// DrawOp represents the drawing operations to be performed when displaying a figure.
// Different operations may be or'ed together to create a single operations specifying multiple operations.
type DrawOp uint8

// Available basic drawing operations.
const (
	Nop     DrawOp = 0 // Used to finish a path, but no drawing is performed.
	Stroke  DrawOp = 1 // Stroke the outline of the shape with the current draw color
	Fill    DrawOp = 2 // Fill the shape with the current fill color
	EvenOdd DrawOp = 4 // Use the even-odd winding rule when determining which regions are part of the interior
)

func (d DrawOp) postscript() string {
	switch d {
	case Fill:
		return "f"
	case Fill | EvenOdd:
		return "f*"
	case Stroke | Fill:
		return "B"
	case Stroke | Fill | EvenOdd:
		return "B*"
	default:
		return "S"
	}
}

// TextRenderMode describes the drawing operations to be applied when displaying text.
// The default mode is FillText.  For details on the other text render modes,
// refer to section 5.2.5 of the reference.
type TextRenderMode uint8

// Available text render modes supported by PDF documents.
const (
	FillText                   TextRenderMode = 0 // Glyph outlines are filled
	StrokeText                 TextRenderMode = 1 // Glyph outlines are stroked
	FillAndStrokeText          TextRenderMode = 2 // Glyph outlines are filled and then stroked
	InvisibleText              TextRenderMode = 3
	FillAndAddToClipping       TextRenderMode = 4
	StrokeAndAddToClipping     TextRenderMode = 5
	FillStrokeAndAddToClipping TextRenderMode = 6
	AddToClippingPath          TextRenderMode = 7 // Glyph outlines are used as a clipping boundary
)

// PushState saves the current graphics state to the graphics stack.
func (p *PDF) PushState() {
	p.currentPage.WriteString("q\n")
}

// PopState restores a graphics state by popping the most recent state from the graphics stack.
func (p *PDF) PopState() {
	p.currentPage.WriteString("Q\n")
}

// WithState isolated drawing commands that might affect the graphics state by pushing the state
// prior to the callback, and then popping it afterwards.
//
// Drawing operations on the PDF are not allowed to add pages, or perform any other action that might
// change current page.
func (p *PDF) WithState(draw func()) {
	// Save the current page so that we can verify it wasn't changed during
	// the callback.
	currentPage := p.currentPage

	p.currentPage.WriteString("q\n")
	draw()
	if currentPage != p.currentPage {
		panic("Change of page inside of WithState callback")
	}
	p.currentPage.WriteString("Q\n")
}

// SetCap updates the graphics state for the current page to set the line cap style.
// The line cap style specifies the shape used at the end of all open paths.
func (p *PDF) SetCap(cap CapStyle) {
	fmt.Fprintf(p.currentPage, "%d J\n", cap)
}

// SetCharacterSpacing updates the graphics state for the current page to set additional spacing between characters when displaying
// text.  The units are unscaled text space units.  See section 9.3.2.
func (p *PDF) SetCharacterSpacing(tc float32) {
	fmt.Fprintf(p.currentPage, "%.2f Tc\n", tc)
}

// SetWordSpacing updates the graphics state for the current page to set additional spacing between words when displaying
// text.  The units are unscaled text space units.  See section 9.3.3.
func (p *PDF) SetWordSpacing(tw float32) {
	fmt.Fprintf(p.currentPage, "%.2f Tw\n", tw)
}

// SetTextScaling updates the graphics state for the current page to modify the horizontal scaling when displaying
// text.  The default value is 100.  See section 9.3.4.
func (p *PDF) SetTextScaling(tz float32) {
	fmt.Fprintf(p.currentPage, "%.2f Tz\n", tz)
}

// SetLeading updates the graphics state for the current page to set the text leading.
// See section 9.3.5.
func (p *PDF) SetLeading(leading float32) {
	fmt.Fprintf(p.currentPage, "%f TL\n", leading)
}

// SetTextRender updates the graphics state for the current page to set the text render mode.
// The text render mode indicates the drawing operations used when displaying glyphs.
// See section 9.3.6.
func (p *PDF) SetTextRender(mode TextRenderMode) {
	fmt.Fprintf(p.currentPage, "%d Tr\n", mode)
}

// SetTextRise updates the graphics state for the current page to set the text rise.
// See section 9.3.7.
func (p *PDF) SetTextRise(rise float32) {
	fmt.Fprintf(p.currentPage, "%f Ts\n", rise)
}

// SetDashPattern updates the graphics state for the current page to set the
// pattern of dashes and gaps when stroking a path.
// The array describes the lengths of dashes and gaps when drawing a line.
// The phase specifies a distance into the pattern which indicates the starting point when drawing a new line.
//
// To clear the dash pattern so that further lines are strokes without any gaps, use a nil array.
//
// See section 8.4.3.6.
func (p *PDF) SetDashPattern(array []PT, phase PT) {
	if len(array) == 0 {
		p.currentPage.WriteString("[] 0 d\n")
		return
	}

	fmt.Fprintf(p.currentPage, "[%.2f", array[0])
	for _, v := range array[1:] {
		fmt.Fprintf(p.currentPage, " %.2f", v)
	}
	fmt.Fprintf(p.currentPage, "] %f d\n", phase)
}

// SetDrawCMYK updates the graphics state for the current page to set the draw color.
// The graphics state is also updated to use the CMKY color space.
func (p *PDF) SetDrawCMYK(c, m, y, k uint8) {
	buffer := [4*7 + 2 + 1]byte{}
	s := buffer[:0]
	s = writeColor(s, c)
	s = writeColor(s, m)
	s = writeColor(s, y)
	s = writeColor(s, k)
	s = append(s, 'K', '\n')
	p.currentPage.Write(s)
}

// SetDrawRGB updates the graphics state for the current page to set the draw color.
// The graphics state is also updated to use the RGB color space.
func (p *PDF) SetDrawRGB(r, g, b uint8) {
	if r == g && g == b {
		p.SetDrawGray(r)
	}

	buffer := [3*7 + 3 + 1]byte{}
	s := buffer[:0]
	s = writeColor(s, r)
	s = writeColor(s, g)
	s = writeColor(s, b)
	s = append(s, 'R', 'G', '\n')
	p.currentPage.Write(s)
}

// SetDrawGray updates the graphics state for the current page to set the draw color.
// The graphics state is also updated to use the gray color space.
func (p *PDF) SetDrawGray(gray uint8) {
	buffer := [7 + 2 + 1]byte{}
	s := buffer[:0]
	s = writeColor(s, gray)
	s = append(s, 'G', '\n')
	p.currentPage.Write(s)
}

// SetDrawGray16 updates the graphics state for the current page to set the draw color.
// The graphics state is also updated to use the gray color space.
func (p *PDF) SetDrawGray16(gray uint16) {
	fmt.Fprintf(p.currentPage, "%.4f G\n", float32(gray)/0xFFFF)
}

// SetDrawColor updates the graphics state for the current page to set the draw color.
// The color space is selected based on the color model.
func (p *PDF) SetDrawColor(draw color.Color) {
	if c, ok := draw.(*color.RGBA); ok {
		p.SetDrawRGB(c.R, c.G, c.B)
	} else if c, ok := draw.(*color.Gray); ok {
		p.SetDrawGray(c.Y)
	} else if c, ok := draw.(*color.Gray16); ok {
		p.SetDrawGray16(c.Y)
	} else if c, ok := draw.(*color.CMYK); ok {
		p.SetDrawCMYK(c.C, c.M, c.Y, c.K)
	} else {
		r, g, b, _ := draw.RGBA()
		fmt.Fprintf(p.currentPage, "%.4f %.4f %.4f RG\n", float32(r)/0xFFFF, float32(g)/0xFFFF, float32(b)/0xFFFF)
	}
}

// SetFillCMYK sets the fill color for the current page using a CMYK color space.
func (p *PDF) SetFillCMYK(c, m, y, k uint8) {
	buffer := [4*7 + 2 + 1]byte{}
	s := buffer[:0]
	s = writeColor(s, c)
	s = writeColor(s, m)
	s = writeColor(s, y)
	s = writeColor(s, k)
	s = append(s, 'k', '\n')
	p.currentPage.Write(s)
}

// SetFillRGB sets the fill color for the current page using a RGB color space.
func (p *PDF) SetFillRGB(r, g, b uint8) {
	if r == g && g == b {
		p.SetFillGray(r)
	}

	buffer := [3*7 + 3 + 1]byte{}
	s := buffer[:0]
	s = writeColor(s, r)
	s = writeColor(s, g)
	s = writeColor(s, b)
	s = append(s, 'r', 'g', '\n')
	p.currentPage.Write(s)
}

// SetFillGray sets the fill color for the current page using a gray color space.
func (p *PDF) SetFillGray(gray uint8) {
	buffer := [7 + 2 + 1]byte{}
	s := buffer[:0]
	s = writeColor(s, gray)
	s = append(s, 'g', '\n')
	p.currentPage.Write(s)
}

// SetFillGray16 sets the fill color for the current page using a gray color space.
func (p *PDF) SetFillGray16(gray uint16) {
	fmt.Fprintf(p.currentPage, "%.4f g\n", float32(gray)/0xFFFF)
}

// SetFillColor sets the fill color for the current page.
// The color space is selected based on the color model.
func (p *PDF) SetFillColor(fill color.Color) {
	if c, ok := fill.(*color.RGBA); ok {
		p.SetFillRGB(c.R, c.G, c.B)
	} else if c, ok := fill.(*color.Gray); ok {
		p.SetFillGray(c.Y)
	} else if c, ok := fill.(*color.Gray16); ok {
		p.SetFillGray16(c.Y)
	} else if c, ok := fill.(*color.CMYK); ok {
		p.SetFillCMYK(c.C, c.M, c.Y, c.K)
	} else {
		r, g, b, _ := fill.RGBA()
		fmt.Fprintf(p.currentPage, "%.4f %.4f %.4f rg\n", float32(r)/0xFFFF, float32(g)/0xFFFF, float32(b)/0xFFFF)
	}
}

// SetFlatness updates the graphics state for the current page to set the flatness tolerance.
// The flatness tolerances controls the accuracy used when converting curves to line
// segments for drawing.  See section 4.3.3 and 6.5.1 in the reference.
//
// A value outside of the range [0,100] is an error, and will cause a panic.
//
// See section 10.6.2.
func (p *PDF) SetFlatness(flatness float64) {
	if flatness < 0 || flatness > 100 {
		panic("Flatness must be in the range of 0 to 100")
	}

	fmt.Fprintf(p.currentPage, "%.1f i\n", flatness)
}

// SetJoin updates the graphics state for the current page to set the line join style.
// The line join style specifies the shape used at the corners when paths are stroked.
func (p *PDF) SetJoin(join JoinStyle) {
	fmt.Fprintf(p.currentPage, "%d j\n", join)
}

// SetMiterLimit updates the graphics state for the current page to set a limit for on how far miter joins can extend beyond the line width.
// See section 8.4.3.5.
func (p *PDF) SetMiterLimit(limit PT) {
	fmt.Fprintf(p.currentPage, "%f M\n", limit)
}

// SetLineWidth updates the graphics state for the current page to set the linewidth.
//
// A negative value is an error, and will cause a panic.
func (p *PDF) SetLineWidth(width PT) {
	// Section 8.4.3.2, linewidths less than zero are an error
	if width < 0 {
		panic("Line width must be non-negative")
	}
	fmt.Fprintf(p.currentPage, "%.2f w\n", width)
}

// Shade paints the region's current clipping region according to the specified shading.
func (p *PDF) Shade(gradientIndex int) {
	fmt.Fprintf(p.currentPage, "/G%d sh\n", gradientIndex)
}

// Line draws a line between points (x1, y1) and (x2, y2) on the current page.
// The color, linewidth and cap style are determined from the graphics state.
func (p *PDF) Line(a, b Point) {
	fmt.Fprintf(p.currentPage, "%.2f %.2f m %.2f %.2f l S\n", a.X, a.Y, b.X, b.Y)
}

// Image creates an image resource for the supplied image, and then displays
// that image on the current page in the box specified.  This is a utility
// function to cover calling AddImage followed by ImageByIndex.
func (p *PDF) Image(img image.Image, bounds Rectangle) {
	imageNdx := p.AddImage(img)
	p.ImageByIndex(imageNdx, bounds)
}

// ImageByIndex displays an image on the current page in the box specified.
func (p *PDF) ImageByIndex(index int, bounds Rectangle) {
	fmt.Fprintf(p.currentPage, "q %.5f 0 0 %.5f %.5f %.5f cm /I%d Do Q\n", bounds.Dx(), bounds.Dy(), bounds.Min.X, bounds.Min.Y, index)
}

// Text displays text on the current page using the current fill color.
// The font for the text must be set before calling this method using SetFont.
func (p *PDF) Text(text string, x, y PT) (ok bool) {
	// Need to convert the text to the correct encoding for the current font.
	encoding := p.currentFont.font.Encoding()
	data, ok := encoding.encodeFromUTF8(text)

	// Begin the text object.
	if bytes.HasSuffix(p.currentPage.Bytes(), []byte{'E', 'T', ' '}) {
		p.currentPage.Truncate(p.currentPage.Len() - 3)
	} else {
		p.currentPage.Write([]byte{'B', 'T', ' '})
	}

	// TODO:  Handle encoding to match the font encoding
	fmt.Fprintf(p.currentPage, "%.2f %.2f Td (%s) Tj ET\n", x, y, escape(string(data)))
	if p.currentFont.underline {
		w := p.textWidth(data)
		fmt.Fprintf(p.currentPage, "%.3f %.3f %.3f %.3f re f\n",
			x, y-PT(p.currentFont.up)*p.currentFont.fontSize/1000,
			w, -PT(p.currentFont.ut)*p.currentFont.fontSize/1000)
	}

	return ok
}

// TextWidth calculates the width
func (p *PDF) TextWidth(text string) (width PT, ok bool) {
	// Need to convert the text to the correct encoding for the current font.
	encoding := p.currentFont.font.Encoding()
	data, ok := encoding.encodeFromUTF8(text)
	if !ok {
		return 0, false
	}

	return p.textWidth(data), true
}

func (p *PDF) textWidth(text []byte) PT {
	rw := p.currentFont.font.RuneWidths()

	// TODO:  How should control characters affecting position be handled?
	// Do we want to reset to 0 after \r or \n?

	w := 0
	for _, ch := range text {
		w += int(rw[ch])
	}
	return p.currentFont.fontSize * PT(w) / 1000
}

func encodeRune(emap map[rune]byte, r rune) byte {
	if b, ok := emap[r]; ok {
		return b
	}

	return '?'
}

// TextSplit determines how a string should be split into separate lines based on
// the font specified by the index and font size so that no line is longer than
// the maximum width.
//
// This function does not modify the PDF document.  To add the text to the document,
// call SetFontByIndex to select the correct font, and then use call Text for each
// line.
func (p *PDF) TextSplit(text string, index int, fontSize PT, maxWidth PT) []string {
	// Identify the font based on the index
	font := p.fonts[(index>>1)-1]
	// Need to convert the text to the correct encoding for the current font.
	emap := font.Encoding().Map()
	// TODO:  Do we need better reporting for encoding failures here?

	sep := -1
	rw := font.RuneWidths()
	ret := make([]string, 0, 8)

	for {
		xpos := 0

		for i, ch := range text {
			if ch == '\r' {
				ret = append(ret, text[:i])
				text, sep = text[i+1:], -1
				break
			}
			if ch == ' ' || ch == '\t' {
				sep = i
			}

			xpos += int(rw[encodeRune(emap, ch)])
			if PT(xpos)*fontSize/1000 > maxWidth {
				newLine := text[:sep]
				// If the last byte is a space, trim it off.
				if n := len(newLine); n > 0 && newLine[n-1] == ' ' {
					newLine = newLine[:n-1]
				}

				ret = append(ret, newLine)
				text, sep = text[sep+1:], -1
				// If the first byte is a space, trim it off.
				if len(text) > 0 && text[0] == ' ' {
					text = text[1:]
				}
				break
			}
		}

		if PT(xpos)*fontSize/1000 <= maxWidth {
			ret = append(ret, text)
			return ret
		}
	}
}

// TextFlow display text on the current page using the current fill color.
// The width of the text is contrained, and text will move to a new line
// when required.
func (p *PDF) TextFlow(text string, x, y, maxWidth PT) PT {
	// Need to convert the text to the correct encoding for the current font.
	encoding := p.currentFont.font.Encoding()
	data, _ := encoding.encodeFromUTF8(text)
	// TODO:  Do we need better reporting for encoding failures here?

	sep := -1
	rw := p.currentFont.font.RuneWidths()
	fontSize := p.currentFont.fontSize

	for {
		xpos := 0

		for i, ch := range data {
			if ch == '\r' {
				fmt.Fprintf(p.currentPage, "BT %.2f %.2f Td (%s) Tj ET\n", x, y, escape(string(data[:i])))
				y, data, sep = y-fontSize, data[i+1:], -1
				break
			}
			if ch == ' ' || ch == '\t' {
				sep = i
			}
			xpos += int(rw[ch])
			if PT(xpos)*fontSize/1000 > maxWidth {
				fmt.Fprintf(p.currentPage, "BT %.2f %.2f Td (%s) Tj ET\n", x, y, escape(string(data[:sep])))
				y, data, sep = y-fontSize, data[sep+1:], -1
				break
			}
		}

		if PT(xpos)*fontSize/1000 <= maxWidth {
			fmt.Fprintf(p.currentPage, "BT %.2f %.2f Td (%s) Tj ET\n", x, y, escape(string(data)))
			return y - fontSize
		}
	}
}

// Translate updates the current graphics context with a transformation matrix to translate future drawing operations.
// This is a utility function to simplify calls to Transform.
func (p *PDF) Translate(x, y PT) {
	fmt.Fprintf(p.currentPage, "1 0 0 1 %.2f %.2f cm\n", x, y)
}

// Rotate updates the current graphics context with a transformation matrix to rotate future drawing operations.
// This is a utility function to simplify calls to Transform.
func (p *PDF) Rotate(radians float64) {
	c := math.Cos(radians)
	s := math.Sin(radians)
	fmt.Fprintf(p.currentPage, "%f %f %f %f 0 0 cm\n", c, s, -s, c)
}

// Scale updates the current graphics context with a transformation matrix to scale future drawing operations.
// This is a utility function to simplify calls to Transform.
func (p *PDF) Scale(factor float32) {
	fmt.Fprintf(p.currentPage, "%f 0 0 %f 0 0 cm\n", factor, factor)
}

// Transform updates the current graphics context with the specified transformation matrix.
// Refer to section 8.3 on coordinate systems.
func (p *PDF) Transform(a, b, c, d, e, f float32) {
	fmt.Fprintf(p.currentPage, "%f %f %f %f %f %f cm\n", a, b, c, d, e, f)
}

// SetRenderingIntent update the current graphics context with the specified rendering intent.
// Refer to section 8.6.5.8.
func (p *PDF) SetRenderingIntent(ri RenderingIntent) {
	fmt.Fprintf(p.currentPage, "%s ri\n", ri)
}

// Rectangle displays a rectangle on the current page.  If Fill is included in
// the drawing operations, the the rectangle will be filled using the current
// fill color.  If Stroke is included in the drawing operation, the rectangle will
// be outlined using the current draw color, line width, and join style.
func (p *PDF) Rectangle(x, y, w, h PT, op DrawOp) *Path {
	if op == 0 {
		fmt.Fprintf(p.currentPage, "%.2f %.2f %.2f %.2f re ", x, y, w, h)
		return (*Path)(&p.currentPage.Buffer)
	}
	fmt.Fprintf(p.currentPage, "%.2f %.2f %.2f %.2f re %s\n", x, y, w, h, op.postscript())
	return nil
}

// Path is a handle for the current drawing path.  Note, each page can have only a
// single drawing path current at a time.  Any path should be closed and painted
// prior to starting a new path on the same page.
type Path bytes.Buffer

// Path returns a handle for the current drawing path.
func (p *PDF) Path() *Path {
	return (*Path)(&p.currentPage.Buffer)
}

// CurveTo modifies the current path by appending a bezier curve through the
// control points and ending on the final coordinates.
func (path *Path) CurveTo(cx, cy, dx, dy, x, y PT) {
	if dx == x && dy == y {
		fmt.Fprintf((*bytes.Buffer)(path), "%.5f %.5f %.5f %.5f y ", cx, cy, x, y)
	}
	fmt.Fprintf((*bytes.Buffer)(path), "%.5f %.5f %.5f %.5f %.5f %.5f c ", cx, cy, dx, dy, x, y)
}

// MoveTo starts a new subpath at the specified coordinates.
func (path *Path) MoveTo(x, y PT) {
	fmt.Fprintf((*bytes.Buffer)(path), "%.2f %.2f m ", x, y)
}

// LineTo modifies the current path by appending a line to the specified coordiates.
func (path *Path) LineTo(x, y PT) {
	fmt.Fprintf((*bytes.Buffer)(path), "%.2f %.2f l ", x, y)
}

// Close closes the current subpath.  If necessary, a line segment is appended to the subpath.
//
// As a convenience, drawing operations can be specified directly.  If Fill
// is included in the drawing operations, the the path will be filled using
// the current fill color.  If Stroke is included in the drawing operation,
// the path will be outlined using the current draw color, line width, and join
// style.
//
// If EvenOdd is included in the drawing operation, an even-odd winding rule
// will be used when filling the interior.
//
// If op is zero, the path will be closed, but no drawing operation will be performed.
// The path can then be used to set a clipping region, or drawn latter.
func (path *Path) Close(op DrawOp) {
	if op == 0 {
		(*bytes.Buffer)(path).WriteString("h ")
		return
	}
	fmt.Fprintf((*bytes.Buffer)(path), "h %s\n", op.postscript())
}

// Clip updates the clipping path with the intersection of the current clipping
// path and the current drawing path.
func (path *Path) Clip() {
	(*bytes.Buffer)(path).WriteString("W n\n")
}

// ClipWithEvenOdd updates the clipping path with the intersection of the current clipping
// path and the current drawing path.  The interior of the current path is
// determined using the even-odd winding rule.
func (path *Path) ClipWithEvenOdd() {
	(*bytes.Buffer)(path).WriteString("W* n\n")
}

// Draw the current drawing path.  If Fill is included in
// the drawing operations, the the path will be filled using the current
// fill color.  If Stroke is included in the drawing operation, the path will
// be outlined using the current draw color, line width, and join style.
//
// If EvenOdd is included in the drawing operation, an even-odd winding rule
// will be used when filling the interior.
func (path *Path) Draw(op DrawOp) {
	fmt.Fprintf((*bytes.Buffer)(path), "%s\n", op.postscript())
}

// Shade fills the the current drawing path with specified shader.
func (path *Path) Shade(gradientIndex int) {
	fmt.Fprintf((*bytes.Buffer)(path), "q W n /G%d sh Q\n", gradientIndex)
}
