package pdf

import (
	"image"
	"image/color"
	"image/draw"
	"image/jpeg"
	"os"
	"testing"
)

func fillBuffer(buffer []byte, value byte) {
	for i := range buffer {
		buffer[i] = value
	}
}

func fillBuffer2(buffer []byte, value1, value2 byte) {
	i := len(buffer) / 2
	fillBuffer(buffer[:i], value1)
	fillBuffer(buffer[i:], value2)
}

func TestImage(t *testing.T) {
	pdf := New(Size{2 * Inch, 2 * Inch})

	title := func(s string) {
		mustSetFont(t, pdf, Helvetica, Normal, 12)
		pdf.Text(s, 12, 2*Inch-12)
	}
	subtitle := func(s string) {
		mustSetFont(t, pdf, Helvetica, Normal, 8)
		pdf.Text(s, 12, 2*Inch-24)
	}

	imageRect := Rect(36, 36, 144-36, 144-36)

	// Gray colour model
	pdf.AddPage()
	title("Gray")
	img1 := image.NewGray(image.Rect(0, 0, 8, 8))
	fillBuffer2(img1.Pix, 64, 192)
	imageNdx := pdf.AddImage(img1)
	pdf.ImageByIndex(imageNdx, imageRect)

	// Gray colour model, gradient
	pdf.AddPage()
	title("Gray")
	img1 = image.NewGray(image.Rect(0, 0, 8, 0x100))
	for i := 0; i < len(img1.Pix); i++ {
		img1.Pix[i] = uint8(i / 8)
	}
	pdf.Image(img1, imageRect)

	// Gray16 colour model
	pdf.AddPage()
	title("Gray16")
	img2 := image.NewGray16(image.Rect(0, 0, 8, 8))
	draw.Draw(img2, image.Rect(0, 0, 8, 4), image.NewUniform(color.RGBA{64, 64, 64, 255}), image.Point{}, draw.Src)
	draw.Draw(img2, image.Rect(0, 4, 8, 8), image.NewUniform(color.RGBA{192, 192, 192, 255}), image.Point{}, draw.Src)
	imageNdx = pdf.AddImage(img2)
	pdf.ImageByIndex(imageNdx, imageRect)

	// Gray16 colour model, gradient
	pdf.AddPage()
	title("Gray16")
	img2 = image.NewGray16(image.Rect(0, 0, 1, 0x1000))
	for i := 0; i < 0x1000; i++ {
		v := uint16(i * 0x10)
		img2.Pix[2*i] = byte(v >> 8)
		img2.Pix[2*i+1] = byte(v)
	}
	pdf.Image(img2, imageRect)

	pdf.AddPage()
	title("Gray16")
	subtitle("(some viewers may fail)")
	img2 = image.NewGray16(image.Rect(0, 0, 1, 0x10000))
	for i := 0; i < 0x10000; i++ {
		img2.Pix[2*i] = byte(i >> 8)
		img2.Pix[2*i+1] = byte(i)
	}
	pdf.Image(img2, imageRect)

	// RGB(A) color model
	pdf.AddPage()
	title("RGBA, no alpha")
	img3 := image.NewRGBA(image.Rect(0, 0, 8, 8))
	draw.Draw(img3, image.Rect(0, 0, 4, 8), image.NewUniform(color.RGBA{0, 128, 0, 255}), image.Point{}, draw.Src)
	draw.Draw(img3, image.Rect(4, 0, 8, 8), image.NewUniform(color.RGBA{0, 0, 192, 255}), image.Point{}, draw.Src)
	imageNdx3 := pdf.AddImage(img3)
	pdf.ImageByIndex(imageNdx3, imageRect)

	// RGB(A) color model.  Utility function to get index and then draw.
	pdf.AddPage()
	title("RGBA, no alpha")
	pdf.Image(img3, imageRect)

	// RGBA color model, using alpha
	pdf.AddPage()
	title("RGBA, using alpha")
	img3 = image.NewRGBA(image.Rect(0, 0, 8, 8))
	draw.Draw(img3, image.Rect(0, 0, 4, 8), image.NewUniform(color.RGBA{0, 128, 0, 128}), image.Point{}, draw.Src)
	draw.Draw(img3, image.Rect(4, 0, 8, 8), image.NewUniform(color.RGBA{0, 0, 128, 128}), image.Point{}, draw.Src)
	mustSetFont(t, pdf, Times, Normal, 72)
	pdf.Text("XOX", 8, 36)
	pdf.Image(img3, Rect(0, 0, 2*Inch, 2*Inch))

	img4, err := func() (image.Image, error) {
		file, err := os.Open("./test1.jpg")
		if err != nil {
			return nil, err
		}
		defer file.Close()

		return jpeg.Decode(file)
	}()
	if err != nil {
		t.Fatalf("Could not get jpeg image: %s", err)
	}
	if img4.ColorModel() != color.YCbCrModel {
		t.Errorf("JPEG should return a YCbCr model for testing")
	}

	pdf.AddPage()
	title("YCbCr")
	subtitle("(via conversion to RGBA)")
	pdf.Image(img4, imageRect)

	// Close and write the PDF file.
	writeFile(t, pdf)
}
