package pdf

import (
	"testing"
)

func TestIsASCII(t *testing.T) {
	cases := []struct {
		in  string
		out bool
	}{
		{"Hello, world!", true},
		{"Bonjour, ça va", false},
		{"Hello, 世界", false},
	}

	for i, v := range cases {
		if out := isASCII(v.in); out != v.out {
			t.Errorf("Error for string %s (%d), want %v, got %v", v.in, i, v.out, out)
		}
	}
}

func TestToUTF16(t *testing.T) {
	cases := []struct {
		in  string
		out []byte
	}{
		{"Hello, world!", []byte{254, 255, 0, 'H', 0, 'e', 0, 'l', 0, 'l', 0, 'o', 0, ',', 0, ' ', 0, 'w', 0, 'o', 0, 'r', 0, 'l', 0, 'd', 0, '!'}},
		{"Bonjour, ça va", []byte{254, 255, 0, 66, 0, 111, 0, 110, 0, 106, 0, 111, 0, 117, 0, 114, 0, 44, 0, 32, 0, 231, 0, 97, 0, 32, 0, 118, 0, 97}},
		{"Hello, 世界", []byte{254, 255, 0, 72, 0, 101, 0, 108, 0, 108, 0, 111, 0, 44, 0, 32, 78, 22, 117, 76}},
	}

	for i, v := range cases {
		if out := toUTF16(v.in); string(out) != string(v.out) {
			t.Errorf("Error for string %s (%d), want %v, got %v", v.in, i, []byte(v.out), out)
		}
	}
}

func TestToWinANSI(t *testing.T) {
	cases := []struct {
		in  string
		out string
		ok  bool
	}{
		{"Bonjour, ça va?", "Bonjour, \xE7a va?", true},
	}

	for i, v := range cases {
		bytes, ok := WinAnsiEncoding.encodeFromUTF8(v.in)
		if ok != v.ok {
			t.Errorf("Unexpected success or failure when encoding to WinAnsi on case %d", i)
		}
		if ok && string(bytes) != v.out {
			t.Errorf("Encoding error (%d), want %s, got %s", i, v.out, string(bytes))
		}
	}
}
