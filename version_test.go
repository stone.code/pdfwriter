package pdf

import (
	"fmt"
	"testing"
)

func TestVersion_LT(t *testing.T) {
	cases := []struct {
		in           Version
		major, minor uint8
		expected     bool
	}{
		{Version{1, 3}, 1, 3, false},
		{Version{1, 2}, 1, 3, true},
		{Version{1, 4}, 1, 3, false},
		{Version{1, 0}, 1, 3, true},
		{Version{2, 3}, 1, 3, false},
		{Version{2, 2}, 1, 3, false},
		{Version{2, 4}, 1, 3, false},
		{Version{0, 3}, 1, 3, true},
		{Version{0, 2}, 1, 3, true},
		{Version{0, 4}, 1, 3, true},
	}

	for i, v := range cases {
		if out := v.in.LT(v.major, v.minor); out != v.expected {
			t.Errorf("Failed case %d, got %v, expected %v", i, out, v.expected)
		}
	}
}

func ExampleVersion_GT() {
	// Example version for the PDF file.
	v := Version{1, 3}

	// Can we use blending?
	if v.LT(1, 4) {
		fmt.Printf("Blending is unavailable, have %s, need 1.4.\n", v)
	}

	// Output:
	// Blending is unavailable, have 1.3, need 1.4.
}

func ExampleVersion_String() {
	// We can check the name of the known page sizes.
	v := Version{1, 3}
	fmt.Printf("String representation is %s.\n", v)

	// Output:
	// String representation is 1.3.
}
